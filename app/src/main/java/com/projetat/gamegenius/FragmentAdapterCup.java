package com.projetat.gamegenius;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupFour;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupOne;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupThree;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupTwo;

public class FragmentAdapterCup extends FragmentStateAdapter {

    private int numberOfPlayer;

    // Constructor
    public FragmentAdapterCup(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, int numberOfPlayer) {
        super(fragmentManager, lifecycle);
        this.numberOfPlayer = numberOfPlayer;
    }

    // Manage position of tabs
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new FragmentCupGroupOne();
            case 1:
                return new FragmentCupGroupTwo();
            case 2:
                if (numberOfPlayer > 12) {
                    return new FragmentCupGroupThree();
                } else {
                    return new FragmentCupGroupOne(); // Un fragment par défaut ou vide
                }
            case 3:
                if (numberOfPlayer > 12) {
                    return new FragmentCupGroupFour();
                } else {
                    return new FragmentCupGroupOne(); // Un fragment par défaut ou vide
                }
            default:
                return new FragmentCupGroupOne();
        }
    }

    // Manage total of tabs
    @Override
    public int getItemCount() {
        return numberOfPlayer > 12 ? 4 : 2;
    }
}
