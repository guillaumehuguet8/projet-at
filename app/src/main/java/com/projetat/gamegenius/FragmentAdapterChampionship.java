package com.projetat.gamegenius;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipRanking;

public class FragmentAdapterChampionship extends FragmentStateAdapter {

    // Constructor
    public FragmentAdapterChampionship(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    // Manage position of tabs
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0){
            return new FragmentChampionshipRanking();
        }
        if (position == 1){
            return new FragmentChampionshipMatch();
        }
        return new FragmentChampionshipRanking();
    }

    // Manage total of tabs
    @Override
    public int getItemCount() {

        return 2;
    }
}
