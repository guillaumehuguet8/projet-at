package com.projetat.gamegenius.old;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.projetat.gamegenius.R;

public class CustomPopup extends Dialog {

    private String title;
    private String namePlayer1;
    private String namePlayer2;
    private int scorePlayer1;
    private int scorePlayer2;
    private Button comparatorView;
    private final TextView titleView;
    private final TextView namePlayer1View;
    private final TextView namePlayer2View;
    private final EditText scorePlayer1View, scorePlayer2View;



    // Constructor
    public CustomPopup(Context context){
        super(context, androidx.appcompat.R.style.Base_ThemeOverlay_AppCompat_Dialog_Alert);
        setContentView(R.layout.popup_template);
        this.title = "Match";
        this.namePlayer1 = "Joueur 1";
        this.namePlayer2 = "Joueur 2";
        // this.scorePlayer1 = 0;
        // this.scorePlayer2 = 0;
        this.titleView = findViewById(R.id.title);
        this.namePlayer1View = findViewById(R.id.namePlayer1);
        this.namePlayer2View = findViewById(R.id.namePlayer2);
        this.scorePlayer1View = findViewById(R.id.scorePlayer1);
        this.scorePlayer2View = findViewById(R.id.scorePlayer2);
        this.comparatorView = findViewById(R.id.comparator);
    }

    // GETTER
    public Button getButtonComparator(){
        return comparatorView;
    }
    public TextView getNamePlayer1(){return namePlayer1View;}
    public TextView getNamePlayer2(){return namePlayer2View;}
    public EditText getScorePlayer1(){return scorePlayer1View;}
    public EditText getScorePlayer2(){return scorePlayer2View;}

    // SETTER
    public void setTitle (String title){
        this.title = title;
    }
    public void setNamePlayer1 (String namePlayer1){
        this.namePlayer1 = namePlayer1;
    }
    public void setNamePlayer2 (String namePlayer2){
        this.namePlayer2 = namePlayer2;
    }


    public void build() {
        show();
        titleView.setText(title);
        namePlayer1View.setText(namePlayer1);
        namePlayer2View.setText(namePlayer2);

    }

}
