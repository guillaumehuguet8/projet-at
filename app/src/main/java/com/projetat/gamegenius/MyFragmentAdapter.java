package com.projetat.gamegenius;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipRanking;

/*
* Display the right fragment in the right place
* */
public class MyFragmentAdapter extends FragmentStateAdapter {
    public MyFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 1){
            return new FragmentChampionshipMatch();
        }
        if (position == 2){
            return new FragmentChampionshipRanking();
        }
        if (position == 3){
            return new GroupThree();
        }
        return new FragmentChampionshipRanking();
    }

    @Override
    public int getItemCount() {

        return 4;
    }
}
