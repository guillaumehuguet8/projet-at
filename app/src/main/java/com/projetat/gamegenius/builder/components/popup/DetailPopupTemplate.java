package com.projetat.gamegenius.builder.components.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;

import java.util.Objects;

public class DetailPopupTemplate extends Dialog {
    LinearLayout myLayout;
    String title;
    String text;
    String question;
    public static String keyTypeTournament;

    //Constructor
    @SuppressLint("MissingInflatedId")
    public DetailPopupTemplate(Context context, String title, String text, String question){
        super(context, androidx.appcompat.R.style.Base_ThemeOverlay_AppCompat_Dialog_Alert);
        setContentView(R.layout.popup_template_detail);
        this.title = title;
        this.text = text;
        this.question = question;

    }

    /* Pop up detail UI
     * Generate popup detail
     * */
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        myLayout = (LinearLayout) findViewById(R.id.popupTemplateDetail);

        generateUI();

    }

    /* Generate UI design
     * Generate title, text and button
     * */
    private void generateUI(){
        //Title
        CustomText titlePopup = new CustomText(getContext(), this.title, myLayout);
        titlePopup.constructorTitle();

        //Text
        CustomText textPopup = new CustomText(getContext(), this.text, myLayout);
        textPopup.constructorText();

        //Text
        CustomText textConfirmation = new CustomText(getContext(), this.question, myLayout);
        textConfirmation.constructorText();
        textConfirmation.setMarginText(30, 0, 30,60);

        //Button
        CustomButton buttonYes = new CustomButton(getContext(), myLayout, buttonListenerYes);
        buttonYes.constructorButtonStandard("Oui");

        //Button
        CustomButton buttonNo = new CustomButton(getContext(), myLayout, buttonListenerNo);
        buttonNo.constructorButtonStandard("Non");
    }

    /* Listener yes button
     * Changes button color according to user choice
     * Stock a value in keyTypeTournament that will be used to launch the appropriate builder or error message
     * And close popup
     * */
    public final View.OnClickListener buttonListenerYes = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");
            if(Objects.equals(title, FragmentSelectTournament.typeTournament[0])){
                FragmentSelectTournament.tournament.setColorBackgroundButtonStandard(R.drawable.corner_round_validation);
                FragmentSelectTournament.championship.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                FragmentSelectTournament.groupPhase.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                keyTypeTournament = FragmentSelectTournament.typeTournament[0];
            }
            else if (Objects.equals(title, FragmentSelectTournament.typeTournament[1])) {
                FragmentSelectTournament.tournament.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                FragmentSelectTournament.championship.setColorBackgroundButtonStandard(R.drawable.corner_round_validation);
                FragmentSelectTournament.groupPhase.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                keyTypeTournament = FragmentSelectTournament.typeTournament[1];
            }
            else {
                FragmentSelectTournament.tournament.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                FragmentSelectTournament.championship.setColorBackgroundButtonStandard(R.drawable.corner_round_invalidation);
                FragmentSelectTournament.groupPhase.setColorBackgroundButtonStandard(R.drawable.corner_round_validation);
                keyTypeTournament = FragmentSelectTournament.typeTournament[2];
            }
            dismiss();
        }
    };

    /* Listener No button
    * Just close Popup
    * */
    public final View.OnClickListener buttonListenerNo = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");
            dismiss();
        }
    };
}
