package com.projetat.gamegenius.builder.components.tools;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.projetat.gamegenius.builder.components.custom.CustomButton;

public class LinearLayoutHorizontalButton {

    public static CustomButton buttonValidation;
    public static CustomButton buttonReset;
    /* INITIALISATION */
    Context context;
    LinearLayout myLayout;
    View.OnClickListener buttonListener1;
    View.OnClickListener buttonListener2;
    LinearLayout linearLayoutHorizontalScore;
    //CustomButton buttonValidation;

    /* CONSTRUCTOR */
    public LinearLayoutHorizontalButton(Context context, LinearLayout myLayout, View.OnClickListener buttonListener1, View.OnClickListener buttonListener2) {
        this.context = context;
        this.myLayout = myLayout;
        this.buttonListener1 = buttonListener1;
        this.buttonListener2 = buttonListener2;
    }

    /*GETTER*/

    /*SETTER*/



    /*METHOD*/

    /* Constructor LinearLayout Horizontal with 2 buttons
     * Create LinearLayout with attribute horizontal
     * Display 2 buttons, validation score / reset
     * Add linearLayoutHorizontal at Layout parent
     * */
    public void constructorLayoutHorizontalButton() {
        // Creating the horizontal LinearLayout
        linearLayoutHorizontalScore = new LinearLayout(this.context);
        linearLayoutHorizontalScore.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(50, 20, 50, 20);
        linearLayoutHorizontalScore.setLayoutParams(layoutParams);
        Log.i("INFO", "constructorLayoutHorizontalButton OK");

        //Button validation
        buttonValidation = new CustomButton(context, linearLayoutHorizontalScore, buttonListener1);
        buttonValidation.constructorButtonScoreValidation("Valider les scores");
        Log.i("INFO", "buttonValidation OK");

        // Button Reset
        buttonReset = new CustomButton(context, linearLayoutHorizontalScore, buttonListener2);
        buttonReset.constructorButtonReset();
        Log.i("INFO", "buttonReset OK");

        // Add view in layout
        myLayout.addView(linearLayoutHorizontalScore);
    }
}
