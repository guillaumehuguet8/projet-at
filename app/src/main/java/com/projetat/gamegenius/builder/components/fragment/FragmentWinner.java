package com.projetat.gamegenius.builder.components.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityCup;
import com.projetat.gamegenius.builder.components.activity.ActivityTournament;
import com.projetat.gamegenius.builder.components.activity.MainActivity;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

public class FragmentWinner extends Fragment {

    View rootView;
    LinearLayout myLayout;
    String keyTournament = DetailPopupTemplate.keyTypeTournament;
    LinearLayout.LayoutParams viewParamsPicture;
    ImageView picture;

    public FragmentWinner() {
        // Le fragment a besoin d'un constructeur vide pour fonctionner
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        assert getActivity() != null;
        getActivity().setTitle(FragmentHomePage.inputNameTournament);

        rootView = inflater.inflate(R.layout.fragment_winner, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès 1");
            myLayout = rootView.findViewById(R.id.fragmentWinner);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès 2");

                // Titre Congratulation
                CustomText congratulation = new CustomText(requireContext(), "Félicitation !", myLayout);
                congratulation.constructorTitle();

                // Picture Congratulation
                constructorPictureCongratulation(requireContext(), myLayout);

                // Name of Winner and text
                if (Objects.equals(keyTournament, FragmentSelectTournament.typeTournament[0])){

                    // Adjustment margin
                    congratulation.setMarginTitle(0,150,0,0);

                    // Name of Winner
                    CustomText nameOfWinner = new CustomText(requireContext(), ActivityTournament.playerNameWinner[0], myLayout);
                    nameOfWinner.constructorSubtitle();

                    // Text
                    CustomText text = new CustomText(requireContext(),"est le grand gagnant du Tournoi !", myLayout);
                    text.constructorText();
                }
                else if (Objects.equals(keyTournament, FragmentSelectTournament.typeTournament[1])){

                    // Adjustment margin
                    congratulation.setMarginTitle(0,50,0,0);

                    // Name of Winner
                    String nameWinner = (String) FragmentChampionshipRanking.arrayRanking[0][0];
                    CustomText nameOfWinner = new CustomText(requireContext(), nameWinner, myLayout);
                    nameOfWinner.constructorSubtitle();

                    // Text
                    int score = (int) FragmentChampionshipRanking.arrayRanking[0][1];
                    CustomText text = new CustomText(requireContext(),"a fini 1er du Championnat,\n Avec un total de " + score + " points !", myLayout);
                    text.constructorText();
                }
                else {

                    // Adjustment margin
                    congratulation.setMarginTitle(0,150,0,0);

                    // Name of Winner
                    CustomText nameOfWinner = new CustomText(requireContext(), ActivityTournament.playerNameWinner[0], myLayout);
                    nameOfWinner.constructorSubtitle();

                    // Text
                    CustomText text = new CustomText(requireContext(),"a remporté la Coupe !", myLayout);
                    text.constructorText();
                }

                // Emplacement du classement
                if (Objects.equals(keyTournament, FragmentSelectTournament.typeTournament[1])){
                    FragmentChampionshipRanking.generateHeaderRanking(requireContext(), myLayout);
                    FragmentChampionshipRanking.generateRanking(requireContext(), myLayout);
                }

                // Button reset
                CustomButton reset = new CustomButton(requireContext(), myLayout, buttonListenerReset);
                reset.constructorButtonStandard("Reset");

                if (Objects.equals(keyTournament, FragmentSelectTournament.typeTournament[1])){
                    reset.setMarginButtonStandard(50,100,50,20);
                }

                // Button PDF

                // Button reset with same team

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Listener Button Reset
     * Open MainActivity
     * Close this
     * */
    public final View.OnClickListener buttonListenerReset = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué reset");

            // Reset All
            allReset();
            Log.i("INFO", "Reset All");

            // Reset Cache
            clearCache();
            Log.i("INFO", "Reset Cache");

            // Reset SharedPreferences
            clearSharedPreferences();
            //Toast.makeText(ActivityWinner.requireContext(), "Cache and preferences cleared", Toast.LENGTH_LONG).show();
            //Log.i("INFO", "Reset SharedPreferences");

            // Open Main Activity
            Intent mainActivity = new Intent (getContext(), MainActivity.class);
            startActivity(mainActivity);
            Log.i("INFO", "J'ouvre la nouvelle activité");
            assert getActivity() != null;
            getActivity().finish();
        }

    };

    // Reset ALL
    public void allReset(){
        // Reset FragmentHomePage
        FragmentHomePage.inputNameTournament = null;
        Log.i("INFO", "FragmentHomePage");

        // Reset FragmentSelectTournament
        FragmentSelectTournament.inputUserNumberPlayer = null;
        FragmentSelectTournament.convertInputUserNumberPlayer = 0;
        Log.i("INFO", "FragmentSelectTournament");

        // Reset FragmentInputNamePlayer
        FragmentInputNamePlayer.name = null;
        FragmentInputNamePlayer.inputUserList.clear();
        FragmentInputNamePlayer.playerNameList.clear();
        Log.i("INFO", "FragmentInputNamePlayer");

        // Reset ActivityTournament
        Arrays.fill(ActivityTournament.playerNameQualifiedEighth, null);
        Arrays.fill(ActivityTournament.playerNameQualifiedQuarter, null);
        Arrays.fill(ActivityTournament.playerNameQualifiedSemi, null);
        Arrays.fill(ActivityTournament.playerNameQualifiedFinal, null);
        Arrays.fill(ActivityTournament.playerNameWinner, null);
        Log.i("INFO", "ActivityTournament");

        // Reset ActivityCup
        ActivityCup.keyGroup = "";
        Log.i("INFO", "ActivityCup");

        // Reset FragmentCupGroupOneRanking
        FragmentCupGroupOneRanking.listRankingGroupOne.clear();
        Log.i("INFO", "FragmentCupGroupOneRanking");

        // Reset FragmentCupGroupTwoRanking
        FragmentCupGroupTwoRanking.listRankingGroupTwo.clear();
        Log.i("INFO", "FragmentCupGroupTwoRanking");

        // Reset FragmentCupGroupThreeRanking
        FragmentCupGroupThreeRanking.listRankingGroupThree.clear();
        Log.i("INFO", "FragmentCupGroupThreeRanking");

        // Reset FragmentCupGroupFourRanking
        FragmentCupGroupFourRanking.listRankingGroupFour.clear();
        Log.i("INFO", "FragmentCupGroupFourRanking");

        // Reset DetailPopupTemplate
        DetailPopupTemplate.keyTypeTournament = null;
        Log.i("INFO", "DetailPopupTemplate");

        // Reset MatchPopupTemplate
        MatchPopupTemplate.token = 0;
        //MatchPopupTemplate.listRankingGroup.clear();
        Log.i("INFO", "MatchPopupTemplate");
    }

    public void clearCache() {
        try {
            File dir = getContext().getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; children != null && i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void clearSharedPreferences() {
        SharedPreferences preferences = getContext().getSharedPreferences("your_preferences_name", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public void constructorPictureCongratulation(Context context, LinearLayout linearLayout){
        // Initialisation from ImageView
        picture = new ImageView(context);

        // Set up LayoutParams
        viewParamsPicture = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up Margin
        viewParamsPicture.setMargins(50, 100, 50, 16);
        picture.setLayoutParams(viewParamsPicture);

        // Set up picture
        picture.setImageResource(R.drawable.congratulation);

        // Add view in layout
        linearLayout.addView(picture);
    }
}
