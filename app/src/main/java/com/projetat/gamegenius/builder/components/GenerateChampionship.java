package com.projetat.gamegenius.builder.components;

import android.util.Log;

public class GenerateChampionship {
    /*INITIALISATION*/
    private int numberOfPlayer;

    /*CONSTRUCTOR*/
    public GenerateChampionship(int numberOfPlayer){
        this.numberOfPlayer = numberOfPlayer;
    }

    /*GETTER*/

    /*SETTER*/

    /*METHOD*/

    public void constructChampionShip(int numberOfPlayer){
        Log.i("INFO", "Je construit un clasement avec : "+numberOfPlayer+"ligne.");
    }
}
