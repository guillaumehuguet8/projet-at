package com.projetat.gamegenius.builder.director;

import com.projetat.gamegenius.builder.builders.Builder;
import com.projetat.gamegenius.builder.components.*;

public class Director {
    /* IMPLEMENTATION */


    /* CONSTRUCTOR */


    public void constructTournament(Builder builder, String nameOfTournament, int numberOfPlayer){
        builder.setNameTournament(nameOfTournament);
        builder.setNumberOfPlayer(numberOfPlayer);
        builder.setGenerateTournament(new GenerateTournament(8));
        builder.setGenerateChampionship(new GenerateChampionship(8));
    }
}
