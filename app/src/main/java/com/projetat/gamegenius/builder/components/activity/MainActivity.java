package com.projetat.gamegenius.builder.components.activity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.projetat.gamegenius.R;

/* Activity Home
* Designates where fragments should be displayed
* And navigate between them using a graph
* Make navigation user-accessible with a toolbar
* */
public class MainActivity extends AppCompatActivity {

    public MainActivity activity;
    public static Toolbar toolbar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        // Turn off dark mode for this activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            setTheme(R.style.Theme_ProjectAT); // Apply a light theme for APIs below 29
        }

        setContentView(R.layout.activity_main);

        clearSharedPreferences();

        // Retrieve the main layout view
        /*View mainLayout = findViewById(R.id.fragment_container_view_main);

        // Retrieve the Drawable used as background
        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.fond_appli);

        if (backgroundDrawable != null) {
            // Change the opacity of the Drawable (0-255, where 255 is completely opaque)
            backgroundDrawable.setAlpha(128); // 50% opacity

            // Apply the Drawable to the main layout
            mainLayout.setBackground(backgroundDrawable);
        }*/

        // navHostFragment designates the location where fragments are to be displayed
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container_view_main);
        assert navHostFragment != null;

        // navController provides access to the graph (my_nav.xml)
        NavController navController = navHostFragment.getNavController();

        // appBarConfiguration allows you to use NavigationUI
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();

        // Displays a toolbar in the activity
        toolbar = findViewById(R.id.my_toolbar_main);
        setSupportActionBar(toolbar);

        // NavigationUI connects everything together to make the links
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);

        // Disable back button
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {

            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }

    private void clearSharedPreferences() {
        SharedPreferences preferences = getSharedPreferences("match_scores", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear(); // Supprime toutes les données
        editor.apply(); // Ou editor.commit(); pour une opération synchrone
    }
}

/* Ajout des pub
* Penser a changer la polique de confidentialité une fois mis en place
* Choisir le type de pub, je pesne qu'une baniere et une pub plein écran ca peux le faire
* */