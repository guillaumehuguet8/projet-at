package com.projetat.gamegenius.builder.builders;

import com.projetat.gamegenius.builder.components.*;

public interface Builder {
    void setNameTournament(String nameTournament);
    void setNumberOfPlayer(int numberOfPlayer);
    void setGenerateTournament(GenerateTournament generateTournament);
    void setGenerateChampionship(GenerateChampionship generateChampionship);
}
