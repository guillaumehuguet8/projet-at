package com.projetat.gamegenius.builder.components.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.projetat.gamegenius.builder.components.activity.ActivityChampionship;
import com.projetat.gamegenius.builder.components.activity.ActivityCup;
import com.projetat.gamegenius.builder.components.activity.MainActivity;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityTournament;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomInputUser;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;
import com.projetat.gamegenius.builder.components.popup.ErrorPopupTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FragmentInputNamePlayer extends Fragment {
    View rootView;
    LinearLayout myLayout;
    CustomInputUser inputUserNamePlayer;
    public static String name;
    public static List<CustomInputUser> inputUserList;
    public static List<String> playerNameList;

    public FragmentInputNamePlayer() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /* Page input name player
     * Récupère l'endroit ou dois s'afficher le contenu du fragment
     *
     * */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.toolbar.setTitle(FragmentHomePage.inputNameTournament);
        rootView = inflater.inflate(R.layout.fragment_input_name_player, container, false);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentInputNamePlayer);
            if (myLayout != null){

                // Generate component UI
                generateUi(FragmentSelectTournament.convertInputUserNumberPlayer);

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Generate UI component
     * Create title and button
     * Boucle for : create input according to number of player
     * Create input
     * Add input in List
     * */
    private void generateUi(int numberOfPlayer){
        // Title
        CustomText title = new CustomText(requireContext(), "Nom des Joueurs", myLayout);
        title.constructorTitle();
        Log.i("INFO", "Le titre s'affiche bien");

        inputUserList = new ArrayList<>();

        Log.i("INFO", "numberOfPlayer : " + numberOfPlayer);
        for (int input = 1; input <= numberOfPlayer; input++) {
            //Input User Name of Player
            inputUserNamePlayer = new CustomInputUser(requireContext(), myLayout);
            inputUserNamePlayer.constructorEditTextNamePlayer(input);
            inputUserList.add(inputUserNamePlayer);
        }

        // Button
        CustomButton next = new CustomButton(requireContext(), myLayout, buttonListenerNext);
        next.constructorButtonStandard("Suivant");
    }

    /* Listener next button
    * Manage name player
    * Mix the contents of the List
    * Stock and open Activity Tournament
    *
    * */
    public final View.OnClickListener buttonListenerNext = new View.OnClickListener() {
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué next 3");

            // Manage name player
            //manageNamePlayer();

            playerNameList = new ArrayList<>();

            for (CustomInputUser inputUser : inputUserList) {
                String playerName = inputUser.getEditNamePlayer();
                if (Objects.equals(playerName, "")){
                    Log.i("INFO", "je suis dans mon if noEntry");
                    final ErrorPopupTemplate popupError = new ErrorPopupTemplate(requireContext(), "noEntry");
                    popupError.show();
                    playerNameList.clear();
                    return;
                }
                else if (playerNameList.contains(playerName)) {
                    Log.i("INFO", "je suis dans mon if samePlayerName");
                    final ErrorPopupTemplate popupError2 = new ErrorPopupTemplate(requireContext(), "samePlayerName");
                    Log.i("INFO", "je créer la popup");
                    popupError2.show();
                    Log.i("INFO", "je l'affiche");
                    playerNameList.clear();
                    Log.i("INFO", "je clear la list");
                    return;
                }
                else {
                    Log.i("INFO", "je suis dans mon else");
                    playerNameList.add(playerName);
                }
            }

            // Display List console
            for (String playerName : playerNameList) {
                Log.i("INFO", "Nom du joueur = "+playerName);
            }

            // Mix the contents of the List
            Collections.shuffle(playerNameList);
            Log.i("INFO", "/////// LE NOM DES JOUEUR SONT MELANGE");

            // Stock and open Activity Tournament
            if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[0])){

                // Manage data to Tournament
                manageDataTournament(playerNameList);

                // Open activity Tournament
                openActivityTournament(requireContext());
            }

            // Stock and open Activity Championship
            else if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[1])) {

                Log.i("INFO", "FragmentSelectTournament.convertInputUserNumberPlayer = "+FragmentSelectTournament.convertInputUserNumberPlayer);
                Log.i("INFO", "Nom du joueur (FragmentChampionshipRanking.arrayRanking[i][0]) = "+FragmentChampionshipRanking.arrayRanking[0][0]);

                for (int i = 0; i < FragmentSelectTournament.convertInputUserNumberPlayer; i++) {
                    Log.i("INFO", "i = "+i);
                    Log.i("INFO", "Nom du joueur (playerNameList.get(i)) = "+playerNameList.get(i));
                    name = playerNameList.get(i);
                    Log.i("INFO", "Nom du joueur (name) = "+name);
                    FragmentChampionshipRanking.arrayRanking[i][0] = name;
                    Log.i("INFO", "Nom du joueur (FragmentChampionshipRanking.arrayRanking[i][0]) = "+FragmentChampionshipRanking.arrayRanking[i][0]);
                    FragmentChampionshipRanking.arrayRanking[i][1] = 0;
                    Log.i("INFO", "Nom du joueur (FragmentChampionshipRanking.arrayRanking[i][1]) = "+FragmentChampionshipRanking.arrayRanking[i][1]);
                    FragmentChampionshipRanking.arrayRanking[i][2] = 0;
                    Log.i("INFO", "Nom du joueur (FragmentChampionshipRanking.arrayRanking[i][1]) = "+FragmentChampionshipRanking.arrayRanking[i][2]);
                }

                // Affichage du tableau trié dans les logs Android
                for (Object[] row : FragmentChampionshipRanking.arrayRanking) {
                    Log.d("arrayRanking", Arrays.toString(row));
                }

                Intent activityChampionship = new Intent (requireContext(), ActivityChampionship.class);
                startActivity(activityChampionship);
                assert getActivity() != null;
                getActivity().finish();
            } else {

                if (FragmentSelectTournament.convertInputUserNumberPlayer < 13){
                    if (FragmentSelectTournament.convertInputUserNumberPlayer % 2 != 0) {
                        // Ajouter un joueur fictif si le nombre de joueurs est impair
                        playerNameList.add("Bye");
                    }
                    int numberPlayer = playerNameList.size();
                    int i = 0;

                    while(i < numberPlayer){
                        Log.i("INFO", "i = "+i);

                        // Ajout player Poule 1
                        Log.i("INFO", "Groupe 1 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupOneRanking.listRankingGroupOne.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;

                        // Ajout player Poule 2
                        Log.i("INFO", "Groupe 2 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupTwoRanking.listRankingGroupTwo.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;
                    }
                } else {

                    if (FragmentSelectTournament.convertInputUserNumberPlayer == 13 || FragmentSelectTournament.convertInputUserNumberPlayer == 17) {
                        // Ajouter un joueur fictif si le nombre de joueurs est impair
                        playerNameList.add("Bye");
                        playerNameList.add("Bye");
                        playerNameList.add("Bye");
                    }

                    if (FragmentSelectTournament.convertInputUserNumberPlayer == 14 || FragmentSelectTournament.convertInputUserNumberPlayer == 18) {
                        // Ajouter un joueur fictif si le nombre de joueurs est impair
                        playerNameList.add("Bye");
                        playerNameList.add("Bye");
                    }

                    if (FragmentSelectTournament.convertInputUserNumberPlayer == 15 || FragmentSelectTournament.convertInputUserNumberPlayer == 19) {
                        // Ajouter un joueur fictif si le nombre de joueurs est impair
                        playerNameList.add("Bye");
                    }

                    int numberPlayer = playerNameList.size();
                    int i = 0;

                    while(i < numberPlayer){
                        Log.i("INFO", "i = "+i);

                        // Ajout player Poule 1
                        Log.i("INFO", "Groupe 1 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupOneRanking.listRankingGroupOne.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;

                        // Ajout player Poule 2
                        Log.i("INFO", "Groupe 2 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupTwoRanking.listRankingGroupTwo.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;

                        // Ajout player Poule 3
                        Log.i("INFO", "Groupe 3 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupThreeRanking.listRankingGroupThree.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;

                        // Ajout player Poule 4
                        Log.i("INFO", "Groupe 4 : Nom du joueur = "+playerNameList.get(i));
                        name = playerNameList.get(i);
                        if (!name.equals("Bye")){
                            FragmentCupGroupFourRanking.listRankingGroupFour.add(new ArrayList<>(List.of(name, 0, 0)));
                        }

                        i++;
                    }
                }

                Intent activityCup = new Intent (requireContext(), ActivityCup.class);
                startActivity(activityCup);
                assert getActivity() != null;
                getActivity().finish();
            }
        }
    };

    /* Manage Name Player
     * For-each convert to String
     * If the value is null, display pop up and clear List
     * Else if the value is contains in List, display pop up and clear List
     * else add value in List
     * */
    public void manageNamePlayer(){
        for (CustomInputUser inputUser : inputUserList) {
            String playerName = inputUser.getEditNamePlayer();
            if (Objects.equals(playerName, "")){
                Log.i("INFO", "je suis dans mon if noEntry");
                final ErrorPopupTemplate popupError = new ErrorPopupTemplate(requireContext(), "noEntry");
                popupError.show();
                playerNameList.clear();
                return;
            }
            else if (playerNameList.contains(playerName)) {
                Log.i("INFO", "je suis dans mon if samePlayerName");
                final ErrorPopupTemplate popupError2 = new ErrorPopupTemplate(requireContext(), "samePlayerName");
                Log.i("INFO", "je créer la popup");
                popupError2.show();
                Log.i("INFO", "je l'affiche");
                playerNameList.clear();
                Log.i("INFO", "je clear la list");
                return;
            }
            else {
                Log.i("INFO", "je suis dans mon else");
                playerNameList.add(playerName);
            }
        }
    }

    /* Manage Data Tournament
     * Retrieves data from the List
     * Sends it to different Arrays depending on the number of player
     * */
    public void manageDataTournament(List<String> list){
        for (int i = 0; i < FragmentSelectTournament.convertInputUserNumberPlayer; i++) {
            Log.i("INFO", "Nom du joueur = "+list.get(i));

            name = list.get(i);
            if (FragmentSelectTournament.convertInputUserNumberPlayer == 16){
                ActivityTournament.playerNameQualifiedEighth[i]= name;

            } else if (FragmentSelectTournament.convertInputUserNumberPlayer == 8){
                ActivityTournament.playerNameQualifiedQuarter[i]= name;

            } else if (FragmentSelectTournament.convertInputUserNumberPlayer == 4){
                ActivityTournament.playerNameQualifiedSemi[i]= name;

            } else {
                ActivityTournament.playerNameQualifiedFinal[i]= name;
            }
        }

        //Log.i("INFO", "tbl : "+Arrays.toString(ActivityTournament.playerNameQualifiedEighth));
        //Log.i("INFO", "tbl : "+Arrays.toString(ActivityTournament.playerNameQualifiedQuarter));
        //Log.i("INFO", "tbl : "+Arrays.toString(ActivityTournament.playerNameQualifiedSemi));
        //Log.i("INFO", "tbl : "+Arrays.toString(ActivityTournament.playerNameQualifiedFinal));
    }

    /* Open Activity Tournament
    * Declares an intent
    * Call startActivity
    * Check that the activity is different from null
    * Open activity Tournament
    * Close MainActivity
    * */
    public void openActivityTournament(Context context){

        Intent activityTournament = new Intent (requireContext(), ActivityTournament.class);
        startActivity(activityTournament);
        assert getActivity() != null;
        getActivity().finish();
    }

    public void printArray(Object[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                Log.d("ArrayGroup", "array[" + i + "][" + j + "] = " + array[i][j]);
            }
        }
    }
}