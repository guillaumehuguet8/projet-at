package com.projetat.gamegenius.builder.tournament;

import android.util.Log;
import android.widget.TextView;

import com.projetat.gamegenius.builder.components.*;

public class Tournament {
    /*INITIALISATION*/
    private String nameTournament;
    private int numberOfPlayer;
    private GenerateTournament generateTournament;
    private GenerateChampionship generateChampionship;
    private TextView lblResult;

    /*CONSTRUCTOR*/
    public Tournament(String nameTournament, int numberOfPlayer, GenerateTournament generateTournament, GenerateChampionship generateChampionship){
        this.nameTournament = nameTournament;
        this.numberOfPlayer = numberOfPlayer;
        this.generateTournament = generateTournament;
        this.generateChampionship = generateChampionship;
    }

    /*GETTER*/
    public String getNameTournament(){

        return this.nameTournament;
    }
    public int getNumberOfPlayer(){

        return numberOfPlayer;
    }

    public GenerateTournament getGenerateTournament(){

        return generateTournament;
    }

    public GenerateChampionship getGenerateChampionship(){

        return generateChampionship;
    }

    /*SETTER*/

    /*METHOD*/

    /*
    A therme cette method doit renvoyer des String pour l'affichage
     */
    public void displayNameOfTournament(){

    }

    /*
    A therme cette method doit générer des champs utilisateurs
    pour la saisie des noms des différents joueurs
     */
    public void inputNameofPlayer(int numberOfPlayer){
        Log.i("INFO", "Je génére des champs utilisateurs pour " +numberOfPlayer+ " joueurs");
    }
}
