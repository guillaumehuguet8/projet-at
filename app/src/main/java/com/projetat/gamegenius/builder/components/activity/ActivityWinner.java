package com.projetat.gamegenius.builder.components.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.fragment.FragmentHomePage;

public class ActivityWinner extends AppCompatActivity {

    private LinearLayout myLayout;
    NavController navController;


    /* Activity Tournament
     * Connects class to the Layout
     * Display Title, Subtitle, text, and Button reset, PDF and reset with same team
     * */
    @SuppressLint({"MissingInflatedId", "CutPasteId"})
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        // Turn off dark mode for this activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            setTheme(R.style.Theme_ProjectAT); // Apply a light theme for APIs below 29
        }

        setContentView(R.layout.activity_winner);
        Log.i("INFO", "Notre activité c'est lancé correctement");

        // Display background
        View championshipLayout = findViewById(R.id.fragment_container_view_winner);
        // Retrieve the Drawable used as background
        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.fond_appli);
        // Apply the Drawable to the main layout
        championshipLayout.setBackground(backgroundDrawable);

        // Retrieve activity layout
        myLayout = (LinearLayout) findViewById(R.id.winner);

        // Display title tournament
        TextView titleWinner = findViewById(R.id.titleWinner);
        titleWinner.setText(FragmentHomePage.inputNameTournament);

        // navHostFragment designates the location where fragments are to be displayed
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container_view_winner);

        if (navHostFragment != null) {
            // navController provides access to the graph (my_nav.xml)
            navController = navHostFragment.getNavController();

        } else {
            Log.i("INFO", "NavHostFragment est null");
        }

        // Disable back button
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // Do nothing to disable the back button
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);

    }
}
