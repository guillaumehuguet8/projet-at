package com.projetat.gamegenius.builder.components.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.projetat.gamegenius.FragmentAdapterCup;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.fragment.FragmentHomePage;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.google.android.material.tabs.TabLayout;

public class ActivityCup extends AppCompatActivity {
    private LinearLayout myLayout;
    private TabLayout tabLayoutCup;
    private ViewPager2 viewFragmentCup;
    private FragmentAdapterCup adapter;
    public static String keyGroup;
    int numberOfPlayer;

    @SuppressLint({"MissingInflatedId", "CutPasteId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Turn off dark mode for this activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            setTheme(R.style.Theme_ProjectAT); // Apply a light theme for APIs below 29
        }

        setContentView(R.layout.activity_cup);

        // Display background
        View cupLayout = findViewById(R.id.viewFragmentCup);
        // Retrieve the Drawable used as background
        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.fond_appli);
        // Apply the Drawable to the main layout
        cupLayout.setBackground(backgroundDrawable);

        // Retrieve activity layout
        myLayout = (LinearLayout) findViewById(R.id.activityCup);

        // Display title tournament
        TextView titleCup = findViewById(R.id.titleCup);
        titleCup.setText(FragmentHomePage.inputNameTournament);

        // Displays a toolbar in the activity
        tabLayoutCup = findViewById(R.id.tabLayoutCup);
        viewFragmentCup = findViewById(R.id.viewFragmentCup);

        // Displays tabs
        tabLayoutCup.addTab(tabLayoutCup.newTab().setText("Poule 1"));
        tabLayoutCup.addTab(tabLayoutCup.newTab().setText("Poule 2"));

        // Display tabs for playeurs superior at 12
        numberOfPlayer = FragmentSelectTournament.convertInputUserNumberPlayer;
        if (numberOfPlayer > 12){
            tabLayoutCup.addTab(tabLayoutCup.newTab().setText("Poule 3"));
            tabLayoutCup.addTab(tabLayoutCup.newTab().setText("Poule 4"));
        }

        // Fragment display management
        FragmentManager fragmentManager = getSupportFragmentManager();
        adapter = new FragmentAdapterCup(fragmentManager , getLifecycle(), numberOfPlayer);
        viewFragmentCup.setAdapter(adapter);

        //Listener that reacts to tab selection events
        tabLayoutCup.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            /* On Tab Selected
            * Updates the current event
            * Call méthode updateKeyGroup()
            * */
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewFragmentCup.setCurrentItem(tab.getPosition());
                updateKeyGroup(tab.getPosition());
            }

            /* On Tab Unselected
             *
             * */
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            /* On Tab Reselected
             *
             * */
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Synchronize tab selection with updateKeyGroup()
        viewFragmentCup.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayoutCup.selectTab(tabLayoutCup.getTabAt(position));
                updateKeyGroup(position);
            }
        });

        // Disable back button
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // Do nothing to disable the back button
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);

    }
    // Update KEY_GROUP
    private void updateKeyGroup(int position) {
        switch (position) {
            case 1:
                keyGroup = "Group_two";
                break;
            case 2:
                keyGroup = "Group_three";
                break;
            case 3:
                keyGroup = "Group_four";
                break;
            default:
                keyGroup = "Group_one";
                break;
        }
    }
}
