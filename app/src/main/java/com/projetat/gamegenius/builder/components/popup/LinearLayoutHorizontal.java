package com.projetat.gamegenius.builder.components.popup;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.custom.CustomText;

public class LinearLayoutHorizontal {

    /* INITIALISATION */
    Context context;
    LinearLayout myLayout;
    LinearLayout linearLayoutHorizontal;
    LinearLayout linearLayoutHorizontalRank;
    String txtRank;
    String txtName;
    String txtPoints;
    String txtScoreForHim;
    CustomText textRank;
    CustomText textName;
    CustomText textPoints;
    CustomText textScoreForHim;

    /* Constructor */
    public LinearLayoutHorizontal(Context context, LinearLayout myLayout, String txtRank, String txtName, String txtPoints, String txtScoreForHim) {
        this.context = context;
        this.myLayout = myLayout;
        this.txtRank = txtRank;
        this.txtName = txtName;
        this.txtPoints = txtPoints;
        this.txtScoreForHim = txtScoreForHim;
    }

    /*GETTER*/

    /*SETTER*/
    public void setBackgroundLineRanking(int drawable) {
        textRank.setBackgroundTextRank(drawable);
        textName.setBackgroundTextRankName(drawable);
        textPoints.setBackgroundTextRank(drawable);
        textScoreForHim.setBackgroundTextRank(drawable);
    }

    public LinearLayout getView() {
        return this.linearLayoutHorizontalRank;
    }


    /*METHOD*/

    /* Constructor LinearLayout Horizontal
     * Create LinearLayout with attribute horizontal
     * Display 4 CustomText for header Ranking
     * Add linearLayoutHorizontal at Layout parent
     * */
    public void constructorHeaderRanking() {
        // Creating the horizontal LinearLayout
        linearLayoutHorizontal = new LinearLayout(this.context);
        linearLayoutHorizontal.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutHorizontal.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        Log.i("INFO", "linearLayoutHorizontal OK");

        // Creation of the 4 boxes of the line

        // Text Header Rank
        CustomText textHeaderRank = new CustomText(this.context, this.txtRank, linearLayoutHorizontal);
        textHeaderRank.constructorHeaderTextRank();
        Log.i("INFO", "textHeaderRank OK");

        // Text Header Name
        CustomText textHeaderName = new CustomText(this.context, this.txtName, linearLayoutHorizontal);
        textHeaderName.constructorHeaderTextRankName();
        Log.i("INFO", "textHeaderName OK");

        // Text Header Points
        CustomText textHeaderPoints = new CustomText(this.context, this.txtPoints, linearLayoutHorizontal);
        textHeaderPoints.constructorHeaderTextRank();
        Log.i("INFO", "textHeaderScore OK");

        // Text Header Score For Him
        CustomText textHeaderScoreForHim = new CustomText(this.context, this.txtScoreForHim, linearLayoutHorizontal);
        textHeaderScoreForHim.constructorHeaderTextRank();
        Log.i("INFO", "textHeaderScoreForHim OK");

        myLayout.addView(linearLayoutHorizontal);
    }

    /* Constructor Line Ranking
     * Construct LinearLayout Horizontal
     * Create LinearLayout with attribute horizontal
     * Display 3 CustomText for header Ranking
     * Add linearLayoutHorizontal at Layout parent
     * */
    public void constructorLineRanking() {
        // Creating the horizontal LinearLayout
        linearLayoutHorizontalRank = new LinearLayout(this.context);
        linearLayoutHorizontalRank.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutHorizontalRank.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        Log.i("INFO", "linearLayoutHorizontalRank OK");

        // Creation of the 4 boxes of the line

        // Text Rank
        textRank = new CustomText(this.context, this.txtRank, linearLayoutHorizontalRank);
        textRank.constructorTextRank();
        textRank.setBackgroundTextRank(R.drawable.rectangle_ranking);
        Log.i("INFO", "textRank OK");

        // Text Name
        textName = new CustomText(this.context, this.txtName, linearLayoutHorizontalRank);
        textName.constructorTextRankName();
        textName.setBackgroundTextRankName(R.drawable.rectangle_ranking);
        Log.i("INFO", "textName OK");

        // Text Points
        textPoints = new CustomText(this.context, this.txtPoints, linearLayoutHorizontalRank);
        textPoints.constructorTextRank();
        textPoints.setBackgroundTextRank(R.drawable.rectangle_ranking);
        Log.i("INFO", "textScore OK");

        // Text Header Score For Him
        textScoreForHim = new CustomText(this.context, this.txtScoreForHim, linearLayoutHorizontalRank);
        textScoreForHim.constructorTextRank();
        textScoreForHim.setBackgroundTextRank(R.drawable.rectangle_ranking);
        Log.i("INFO", "textScoreForHim OK");

        myLayout.addView(linearLayoutHorizontalRank);
    }



}

/* Ajouter le scores total du joueur
* Le but et de départager les joueur en cas d'égalité de point
* Partie front :
* -> Ajouter une case dans le header intitulé "Score" et changer la case qui s'appelle "Scores" par "Points"
* -> Ajouter une case après les points ou s'affiche la somme total du score du joueur
* Partie back :
* -> Ajouté une colonne au tableau en 2D qui récupère les scores des joueurs une fois que le btn validé
* -> Pour le btn reset il faudrait empéché la saisie de nouveau score tant que le btn reset n'a pas été pressé
* car dans l'algo nous aurons besoin de récupérer les scores saisie pour les soustraires en cas d'erreur
* -> Pour stocké le score du joueur faire comme les points
* -> Ajouter une condition qui compare les scores des des joueur pour que le joueur avec le plus haut score passe devant
* */
