package com.projetat.gamegenius.builder.components.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.projetat.gamegenius.FragmentAdapterChampionship;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.fragment.FragmentHomePage;
import com.google.android.material.tabs.TabLayout;

public class ActivityChampionship extends AppCompatActivity {
    private LinearLayout myLayout;
    private TabLayout tabLayout;
    private ViewPager2 viewFragmentChampionship;
    private FragmentAdapterChampionship adapter;

    public static Toolbar toolbar;

    /* Activity Championship
     * Display title tournament
     * Displays a toolbar in the activity for display tabs
     * Fragment display and fragment lifecycle management
     * Synchronize tabs
     * */
    @SuppressLint({"MissingInflatedId", "CutPasteId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Turn off dark mode for this activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            setTheme(R.style.Theme_ProjectAT); // Apply a light theme for APIs below 29
        }

        setContentView(R.layout.activity_championship);

        // Display background
        View winnerLayout = findViewById(R.id.viewFragmentChampionship);
        // Retrieve the Drawable used as background
        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.fond_appli);
        // Apply the Drawable to the main layout
        winnerLayout.setBackground(backgroundDrawable);

        // Retrieve activity layout
        myLayout = (LinearLayout) findViewById(R.id.activityChampionship);

        // Display title tournament
        TextView titleChampionship = findViewById(R.id.titleChampionship);
        titleChampionship.setText(FragmentHomePage.inputNameTournament);

        // Displays a toolbar in the activity
        tabLayout = findViewById(R.id.tabLayout);
        viewFragmentChampionship = findViewById(R.id.viewFragmentChampionship);

        // Displays tabs
        tabLayout.addTab(tabLayout.newTab().setText("Classement"));
        tabLayout.addTab(tabLayout.newTab().setText("Matchs"));

        // Fragment display and fragment lifecycle management
        FragmentManager fragmentManager = getSupportFragmentManager();
        adapter = new FragmentAdapterChampionship(fragmentManager , getLifecycle());
        viewFragmentChampionship.setAdapter(adapter);

        // Synchronize tabs
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewFragmentChampionship.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Ensures tab changeover
        viewFragmentChampionship.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

        /*
        * Disable back button
        * */
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // Do nothing to disable the back button
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }
}
