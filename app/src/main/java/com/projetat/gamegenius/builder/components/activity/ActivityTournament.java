package com.projetat.gamegenius.builder.components.activity;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;

import java.util.Objects;

public class ActivityTournament extends AppCompatActivity {

    public static Toolbar toolbar;
    int numberOfPlayer;
    NavController navController;
    public static String[] playerNameQualifiedEighth = new String[16];
    public static String[] playerNameQualifiedQuarter = new String[8];
    public static String[] playerNameQualifiedSemi = new String[4];
    public static String[] playerNameQualifiedFinal = new String[2];
    public static String[] playerNameWinner = new String[1];

    /* Activity Tournament Display
     * Designates where fragments should be displayed
     * And navigate between them using a graph
     * Just display toolbar
     * Display fragment depending on number of player
     * */
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        // Turn off dark mode for this activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            setTheme(R.style.Theme_ProjectAT); // Apply a light theme for APIs below 29
        }

        setContentView(R.layout.activity_tournament);

        // Retrieve the main layout view
        View mainLayout = findViewById(R.id.fragment_container_view_tournament);

        // Retrieve the Drawable used as background
        Drawable backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.fond_appli);

        if (backgroundDrawable != null) {
            // Change the opacity of the Drawable (0-255, where 255 is completely opaque)
            backgroundDrawable.setAlpha(128); // 50% opacity

            // Apply the Drawable to the main layout
            mainLayout.setBackground(backgroundDrawable);
        }

        // navHostFragment designates the location where fragments are to be displayed
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container_view_tournament);

        if (navHostFragment != null) {
            // navController provides access to the graph (my_nav.xml)
            navController = navHostFragment.getNavController();

        } else {
            Log.i("INFO", "NavHostFragment est null");
        }

        // Displays a toolbar in the activity
        toolbar = findViewById(R.id.my_toolbar_tournament);
        setSupportActionBar(toolbar);

        Log.i("INFO", "Notre activité c'est lancé correctement");

        // Recover number of player
        numberOfPlayer = FragmentSelectTournament.convertInputUserNumberPlayer;

        // Display fragment
        displayFragmentTournament(numberOfPlayer);

        // Disable back button
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // Do nothing to disable the back button
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }

    /* Method Display Fragment Tournament
     * Display fragment depending on who call ActivityTournament
     * FragmentInputNamePlayer or FragmentCupGroup
     * */
    void displayFragmentTournament(int numberOfPlayer){

        if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[0])){
            displayForTournament(numberOfPlayer);
        }

        else {
            displayForCup(numberOfPlayer);
        }
    }

    /* Method Display For Tournament
     * Display fragment depending on number of player
     * 16 or 8 or 4 or 2
     * */
    void displayForTournament(int numberOfPlayer){

        if(numberOfPlayer == 16) {
            navController.navigate(R.id.fragmentTournamentEighth);

        } else if(numberOfPlayer == 8) {
            navController.navigate(R.id.fragmentTournamentQuarter);

        } else if(numberOfPlayer == 4) {
            navController.navigate(R.id.fragmentTournamentSemi);

        } else {
            navController.navigate(R.id.fragmentTournamentFinal);

        }
    }

    /* Method Display For Cup
     * Display fragment depending on number of player
     * 4 or 8
     * */
    void displayForCup(int numberOfPlayer){

        if (numberOfPlayer == 4){
            navController.navigate(R.id.fragmentTournamentSemi);
        }

        else {
            navController.navigate(R.id.fragmentTournamentQuarter);
        }
    }
}
