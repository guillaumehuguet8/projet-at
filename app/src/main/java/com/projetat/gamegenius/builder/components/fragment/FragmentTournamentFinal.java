package com.projetat.gamegenius.builder.components.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityTournament;
import com.projetat.gamegenius.builder.components.activity.ActivityWinner;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;

public class FragmentTournamentFinal extends Fragment {

    View rootView;
    LinearLayout myLayout;
    public static CustomButton matchButton;
    int numberOfPlayer;

    public static CustomButton next;

    public FragmentTournamentFinal() {
        // Le fragment a besoin d'un constructeur vide pour fonctionner
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    /* Page Eighth of final
     * Recovers name of tournament for display on the toolbar
     * Installation of rootView for display in myLayout
     * Display Title, Buttons of matches (8) and Button next
     * */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        assert getActivity() != null;
        getActivity().setTitle(FragmentHomePage.inputNameTournament);

        rootView = inflater.inflate(R.layout.fragment_tournament_final, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès 1");
            myLayout = rootView.findViewById(R.id.fragmentTournamentFinal);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès 2");

                //Title
                CustomText subtitle = new CustomText(requireContext(), "Finale", myLayout);
                subtitle.constructorSubtitle();
                subtitle.setMarginSubtitle(0,60,0,40);

                // Create UI Button
                generateMatch(requireContext(), myLayout);

                // Button Next
                next = new CustomButton(requireContext(), myLayout, buttonListenerNext);
                next.constructorButtonNext();


                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Generate Match
     * Retrieve the number of players
     * Initializes the popup id and the two indexes for retrieval in the Array
     * Until all matches are created
     * Recover player 1, 2 and popup id
     * Create button and call showPopup()
     * Construct the button
     * Adds iterations to the variable
     * */
    private void generateMatch(Context context, LinearLayout myLayout) {
        // récupère le nb de joueur
        numberOfPlayer = FragmentSelectTournament.convertInputUserNumberPlayer;
        int finalButtonId = 0;
        int idPlayerOne = 0;
        int idPlayerTwo = 1;

        for (int i = 0; i < numberOfPlayer / 2; i++){
            // Récupère le joueur 1
            String player1 = ActivityTournament.playerNameQualifiedFinal[idPlayerOne];
            // Récupère le joueur 2
            String player2 = ActivityTournament.playerNameQualifiedFinal[idPlayerTwo];
            // récupère l'id
            int finalButtonId1 = finalButtonId;
            Log.i("INFO", "finalButtonId = "+finalButtonId);
            //créer le button
            matchButton = new CustomButton(context, myLayout, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showPopup(finalButtonId1, context, player1, player2);
                }
            });
            Log.i("INFO", "finalButtonId = "+finalButtonId1);
            matchButton.constructorButtonMatch(player1, player2, finalButtonId1);
            idPlayerOne += 2;
            idPlayerTwo += 2;
            finalButtonId++;
        }
    }

    /* Show Popup
     * Open the match Pop up
     * */
    public static void showPopup(int buttonId, Context context, String playerOne, String playerTwo) {
        Log.i("INFO", "Boutton cliqué match");
        final MatchPopupTemplate popupMatch = new MatchPopupTemplate(context, playerOne, playerTwo, buttonId);
        popupMatch.show();
        Log.i("INFO", "Je génère bien la popup");
    }

    /* Listener Button Next
     * Divide the number of players by 2
     * Reset the contents of SharedPreferences
     * Reset token
     * Open Activity Winner
     * */
    public final View.OnClickListener buttonListenerNext = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué next");

            //Log.i("INFO", "tbl : "+Arrays.toString(FragmentInputNamePlayer.playerNameQualified));
            FragmentSelectTournament.convertInputUserNumberPlayer = FragmentSelectTournament.convertInputUserNumberPlayer/2;
            Log.i("INFO", "Nombre de joueur restant = "+FragmentSelectTournament.convertInputUserNumberPlayer);

            // Reset score
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            for (int idMatch = 0; idMatch < FragmentSelectTournament.convertInputUserNumberPlayer; idMatch++){
                editor.remove("scoreOne_" + idMatch);
                editor.remove("scoreTwo_" + idMatch);
                editor.apply();
            }

            // Reset token
            MatchPopupTemplate.token = 0;

            // Open next Activity
            Intent activityWinner = new Intent (getContext(), ActivityWinner.class);
            startActivity(activityWinner);
            Log.i("INFO", "J'ouvre la nouvelle activité");
            assert getActivity() != null;
            getActivity().finish();
        }
    };
}
