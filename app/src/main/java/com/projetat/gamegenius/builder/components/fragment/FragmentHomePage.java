package com.projetat.gamegenius.builder.components.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.projetat.gamegenius.builder.components.activity.MainActivity;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomInputUser;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.ErrorPopupTemplate;

import java.util.Objects;

public class FragmentHomePage extends Fragment {

    /*INITIALISATION*/
    View rootView;
    LinearLayout myLayout;
    CustomInputUser nameOfTournament;
    public static String inputNameTournament;

    /*CONSTRUCTOR*/
    public FragmentHomePage() {
        // Required empty public constructor
    }

    /*METHOD*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    /*
    * Récupère l'endroit ou dois s'afficher le contenu du fragment
    * Lie ce contenu a un Layout
    * Créer l'affichage du layout
    * */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MainActivity.toolbar.setTitle("Home");

        // Récupère l'endroit ou dois s'afficher le contenu du fragment
        rootView = inflater.inflate(R.layout.fragment_home_page, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");

            //Lie ce contenu a un Layout
            myLayout = rootView.findViewById(R.id.fragmentHomePage);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Logo
                ImageView logo = new ImageView(requireContext());
                logo.setImageResource(R.drawable.logo); // Remplacez "your_image" par le nom de votre image dans le dossier drawable.
                // Définir les nouvelles dimensions en pixels
                int newWidth = 300; // Largeur en pixels
                int newHeight = 300; // Hauteur en pixels
                LinearLayout.LayoutParams logoParams = new LinearLayout.LayoutParams(newWidth, newHeight);
                // Positionner l'image au centre
                logoParams.gravity = Gravity.CENTER;

                // Ajout de marges autour du logo
                logoParams.setMargins(0, 200, 0, 0); // (left, top, right, bottom)

                logo.setLayoutParams(logoParams);
                myLayout.addView(logo); // Ajoutez l'ImageView au layout de votre fragment.

                //Title
                CustomText title = new CustomText(requireContext(), "GameGenius", myLayout);
                title.constructorTitle();
                title.setSizeTitle(40);

                //Subtitle
                CustomText subtitle = new CustomText(requireContext(), "Organisez, Gérez, Gagnez :\nTransformez vos Compétitions en Succès !", myLayout);
                subtitle.constructorSubtitle();

                //Text
                CustomText text = new CustomText(requireContext(), "Bienvenue sur GameGenius, l'application de gestion de Tournoi, Championnat et Coupe.\nVous voulez organiser une compétition entre amis ?\nLaissez-nous simplifier votre gestion et vous aider à faire de chaque tournoi un moment inoubliable.", myLayout);
                text.constructorText();

                //Input user standard
                nameOfTournament = new CustomInputUser(requireContext(), myLayout);
                nameOfTournament.constructorEditText("Nom du Tournoi");

                // Button
                CustomButton next = new CustomButton(requireContext(), myLayout, buttonListener);
                next.constructorButtonStandard("Suivant");

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Listener Button Next
    * Stock input user
    * If input user is void, display popup error
    * Else go to next fragment
    * */
    public final View.OnClickListener buttonListener = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");

            // Stock input user
            inputNameTournament = nameOfTournament.getTextEditText();

            if (Objects.equals(inputNameTournament, "")) {
                //display popup error
                final ErrorPopupTemplate popupError = new ErrorPopupTemplate(requireContext(), "nameTournament");
                popupError.show();
            } else {
                // Go to next fragment
                NavController navController = Navigation.findNavController(rootView);
                navController.navigate(R.id.action_fragmentHomePage_to_fragmentSelectTournament);
                //Intent activityCup = new Intent (requireContext(), ActivityCup.class);
                //startActivity(activityCup);
                //assert getActivity() != null;
                //getActivity().finish();
            }

        }
    };
}