package com.projetat.gamegenius.builder.components.custom;

import android.content.Context;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

public class CustomInputUser {
    /*INITIALISATION*/
    Context activity;
    LinearLayout myLayout;
    EditText userInput;
    EditText inputNamePlayer;
    EditText editNumber;
    LinearLayout.LayoutParams viewParamsEditText;
    LinearLayout.LayoutParams viewParamsNamePlayer;
    LinearLayout.LayoutParams viewParamsNumber;

    // Limit character for user
    int maxLengthTitle = 20;
    int maxLengthNamePlayer = 12;
    int maxLengthNumber = 3;
    InputFilter[] filters = new InputFilter[1];
    int heightInDp;
    int heightInPx;

    /*CONSTRUCTOR*/
    public CustomInputUser(Context activity, LinearLayout myLayout){
        this.activity = activity;
        this.myLayout = myLayout;
    }

    /*GETTER*/
    public String getTextEditText(){
        return userInput.getText().toString();
    }

    public String getEditNumber(){
        return editNumber.getText().toString();
    }
    public String getEditNamePlayer(){
        // inputNamePlayer.getId();
        return inputNamePlayer.getText().toString();
    }

    /*SETTER*/

    public void setTextEditNumber(String score){

        editNumber.setText(score);
    }

    public void setLockOrUnlockInputUserScore(Boolean trueOrFalse){
        editNumber.setEnabled(trueOrFalse);
    }
    /*METHOD*/

    /* Construct EditText basic
     * */
    public void constructorEditText(String hint){

        // Initialisation from EditText
        userInput = new EditText(this.activity);

        // St up height dp
        heightInDp = 50;
        heightInPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, heightInDp, activity.getResources().getDisplayMetrics());

        //TODO: vérifier celui qui est le bon
        // Set up LayoutParams
        viewParamsEditText  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                heightInPx // Hauteur
        );
        // Set up LayoutParams
        /*viewParamsEditText  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );*/

        // Set up position
        viewParamsEditText .setMargins(50, 20, 50, 20);
        userInput.setLayoutParams(viewParamsEditText);

        // Set up Filter (20 characters)
        filters[0] = new InputFilter.LengthFilter(maxLengthTitle);
        userInput.setFilters(filters);

        // Set up background
        userInput.setBackgroundColor(Color.LTGRAY);

        // Set up font

        // Set up text
        userInput.setHint(hint);
        userInput.setTextSize(15);

        // Add view in layout
        myLayout.addView(userInput);
    }

    /* Construct EditText NamePlayer
    Takes the number from the loop and displays it in the hint
     * */
    public void constructorEditTextNamePlayer(int nb){

        // Initialisation from EditText
        inputNamePlayer = new EditText(this.activity);

        // St up height dp
        heightInDp = 50;
        heightInPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, heightInDp, activity.getResources().getDisplayMetrics());

        //TODO: vérifier celui qui est le bon
        // Set up LayoutParams
        viewParamsNamePlayer  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                heightInPx  // Hauteur
        );
        // Set up LayoutParams
        /*viewParamsNamePlayer  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );*/

        // Set up position
        viewParamsNamePlayer .setMargins(50, 20, 50, 20);
        inputNamePlayer.setLayoutParams(viewParamsNamePlayer);

        // Set up Filter (12 characters)
        filters[0] = new InputFilter.LengthFilter(maxLengthNamePlayer);
        inputNamePlayer.setFilters(filters);

        // Set up font

        // Set up text
        inputNamePlayer.setHint("Joueur " + nb);
        inputNamePlayer.setTextSize(15);
        inputNamePlayer.setId(nb);

        // Add view in layout
        myLayout.addView(inputNamePlayer);
    }

    /* Construct EditText of number
     * */
    public void constructorEditNumber(String hint){
        // Initialisation from EditText
        editNumber = new EditText(this.activity);

        // St up height dp
        heightInDp = 50;
        heightInPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, heightInDp, activity.getResources().getDisplayMetrics());

        //TODO: vérifier celui qui est le bon
        // Set up LayoutParams
        viewParamsNumber  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                heightInPx // Hauteur
        );
        // Set up LayoutParams
        /*viewParamsNumber  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );*/

        // Set up position
        viewParamsNumber.setMargins(50, 20, 50, 20);
        editNumber.setLayoutParams(viewParamsNumber);

        // Set up Filter (3 characters)
        filters[0] = new InputFilter.LengthFilter(maxLengthNumber);
        editNumber.setFilters(filters);

        // Set up background
        editNumber.setBackgroundColor(Color.LTGRAY);

        // Set up font

        // Set up text
        editNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        editNumber.setTextSize(15);
        editNumber.setHint(hint);
        editNumber.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        myLayout.addView(editNumber);
    }
}
