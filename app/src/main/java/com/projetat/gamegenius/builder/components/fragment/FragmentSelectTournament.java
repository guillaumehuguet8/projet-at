package com.projetat.gamegenius.builder.components.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.projetat.gamegenius.builder.components.activity.MainActivity;
import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomInputUser;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;
import com.projetat.gamegenius.builder.components.popup.ErrorPopupTemplate;

import java.util.Objects;

public class FragmentSelectTournament extends Fragment {

    View rootView;
    LinearLayout myLayout;
    public static String inputUserNumberPlayer;

    public static int convertInputUserNumberPlayer;

    public static CustomButton tournament;
    public static CustomButton championship;
    public static CustomButton groupPhase;
    CustomInputUser inputNumberOfPlayer;
    public static String[] typeTournament = new String[] {"Tournoi", "Championnat", "Coupe", "Qualify Cup"};
    String[] detailTournament = new String[] {"Les phases qualificatives ne peuvent se jouer qu'avec 2, 4, 8 ou 16 joueurs. Lors du premier tour, les noms des joueurs sont tirés aléatoirement, ensuite, les gagnants montent d'un cran jusqu'à la finale.",
                    "Le Championnat accepte un maximum de 20 joueurs. Tous les joueurs s'affronteront entre eux une fois. Ils pourront gagner des points en fonction du résultat, le gagnant prendra 3 points tandis que le perdant n'en prendra pas. En cas d'égalité un seul point sera distribué à chaque joueur.",
                    "La Coupe accepte un minimum de 4 joueurs et un maximum de 20 joueurs. Tous les joueurs s'affrontent dans leur poule respective. Ils pourront gagner des points en fonction de leur résultat, le gagnant prendra 3 points tandis que le perdant n'en prendra pas. En cas d'égalité un seul point sera distribué à chaque joueur. Une fois les matchs finis, seuls les joueurs qualifiés pourront passer à l'étape suivante (ils sont surlignés dans le classement des poules).)"};

    public FragmentSelectTournament() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /* Page Select Tournament
     * Récupère l'endroit ou dois s'afficher le contenu du fragment
     * Generate UI design
     * */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.toolbar.setTitle(FragmentHomePage.inputNameTournament);
        rootView = inflater.inflate(R.layout.fragment_select_tournament, container, false);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentSelectTournament);
            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Generate design UI
                generatorUi();

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Generate UI
     * Create 2 subtitle
     * Create Button of different type of tournament
     * Create input number of player
     * Create next button
     * */
    private void generatorUi(){
        // Subtitle
        CustomText typeOfTournament = new CustomText(requireContext(), "Type de compétition", myLayout);
        typeOfTournament.constructorSubtitle();

        // Text
        CustomText textTypeOfTournament = new CustomText(requireContext(), "Sélectionnez un type de compétition :", myLayout);
        textTypeOfTournament.constructorText();

        // Button Tournament
        tournament = new CustomButton(requireContext(), myLayout, buttonListenerTournament);
        tournament.constructorButtonStandard("Tournoi");

        // Button Championship
        championship = new CustomButton(requireContext(), myLayout, buttonListenerChampionship);
        championship.constructorButtonStandard("Championnat");

        // Button Cup
        groupPhase = new CustomButton(requireContext(), myLayout, buttonListenerGroupPhase);
        groupPhase.constructorButtonStandard("Coupe");

        //Subtitle
        CustomText SubtitleNumberOfPlayer = new CustomText(requireContext(), "Nombre de Joueurs", myLayout);
        SubtitleNumberOfPlayer.constructorSubtitle();

        // Text
        CustomText textNumberOfPlayer = new CustomText(requireContext(), "Entrez le nombre de participants :", myLayout);
        textNumberOfPlayer.constructorText();

        //Input User Number
        inputNumberOfPlayer = new CustomInputUser(requireContext(), myLayout);
        inputNumberOfPlayer.constructorEditNumber("Ex : 8");

        // Button Next
        CustomButton next = new CustomButton(requireContext(), myLayout, buttonListener);
        next.constructorButtonStandard("Suivant");
    }

    /* Listener next button
     * Transform input number of player at String
     * Verify if user a select type of tournament and/or a number of player
     * Convert number of player String at int
     * Call method Manage Error for Type of Tournament
     * */
    public final View.OnClickListener buttonListener = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué next 2");
            // Transform input number of player at String
            inputUserNumberPlayer = inputNumberOfPlayer.getEditNumber();

            // Management error no select type of tournament
            if (Objects.equals(DetailPopupTemplate.keyTypeTournament, null)) {
                Log.i("INFO", "je suis dans mon if key");
                final ErrorPopupTemplate popupError = new ErrorPopupTemplate(requireContext(), "noTypeOftournament");
                popupError.show();
            }
            // Management error for empty input field
            else if (Objects.equals(inputUserNumberPlayer, "")) {
                Log.i("INFO", "je suis dans mon if number");
                final ErrorPopupTemplate popupError2 = new ErrorPopupTemplate(requireContext(), "numberPlayer");
                popupError2.show();
            }
            else {
                // Convert number of player String at int
                convertInputUserNumberPlayer = Integer.parseInt(inputUserNumberPlayer);

                // Call method
                manageErrorForTypeOfTournament(convertInputUserNumberPlayer);
            }
        }
    };

    /* Manage Error for Type of Tournament
     * Verify if all conditions to launch a tournament type are ok
     * If yes, open next fragment
     * If no, open pop up Error
     * */
    public void manageErrorForTypeOfTournament(int numberOfPlayer){
        // Management error for type Tournament
        if (Objects.equals(DetailPopupTemplate.keyTypeTournament, typeTournament[0]) && numberOfPlayer != 2 && numberOfPlayer != 4 && numberOfPlayer != 8 && numberOfPlayer != 16){
            final ErrorPopupTemplate popupError3 = new ErrorPopupTemplate(requireContext(), "numberPlayerTournament");
            popupError3.show();
        }
        // Management error for type Championship
        else if (Objects.equals(DetailPopupTemplate.keyTypeTournament, typeTournament[1]) && numberOfPlayer > 20) {
            final ErrorPopupTemplate popupError4 = new ErrorPopupTemplate(requireContext(), "numberPlayerHigher");
            popupError4.show();
        }
        // Management error for type GroupPhase
        else if (Objects.equals(DetailPopupTemplate.keyTypeTournament, typeTournament[2]) && numberOfPlayer < 4 || numberOfPlayer > 20) {
            final ErrorPopupTemplate popupError5 = new ErrorPopupTemplate(requireContext(), "numberPlayerHigher");
            popupError5.show();
        }
        // Open next fragment
        else {
            NavController navController = Navigation.findNavController(rootView);
            navController.navigate(R.id.action_fragmentSelectTournament_to_fragmentInputNamePlayer);
        }
    }


    /* Listener Tournament button
    * Open popup for detail of Tournament
    * */
    public final View.OnClickListener buttonListenerTournament = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué tournament");
            final DetailPopupTemplate popupTournament = new DetailPopupTemplate(requireContext(),typeTournament[0], detailTournament[0], "Voulez-vous lancer un "+typeTournament[0]+" ?");
            popupTournament.show();
        }

    };

    /* Listener Championship button
     * Open popup for detail of Championship
     * */
    public final View.OnClickListener buttonListenerChampionship = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");
            final DetailPopupTemplate popupChampionship = new DetailPopupTemplate(requireContext(),typeTournament[1], detailTournament[1], "Voulez-vous lancer un "+typeTournament[1]+" ?");
            popupChampionship.show();
        }
    };

    /* Listener Cup button
     * Open popup for detail of Cup
     * */
    public final View.OnClickListener buttonListenerGroupPhase = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");
            final DetailPopupTemplate popupGroupPhase = new DetailPopupTemplate(requireContext(),typeTournament[2], detailTournament[2], "Voulez-vous lancer un "+typeTournament[2]+" ?");
            popupGroupPhase.show();
        }
    };
}
