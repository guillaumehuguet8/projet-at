package com.projetat.gamegenius.builder.components.custom;

//import static androidx.appcompat.graphics.drawable.DrawableContainerCompat.Api21Impl.getResources;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;

public class CustomButton {
    /*INITIALISATION*/
    Context activity;
    LinearLayout myLayout;
    Button standard;
    Button scoreValidation;
    ImageButton reset;
    Button next;
    Button match;
    View.OnClickListener buttonListener;
    Typeface komikax;
    LinearLayout.LayoutParams viewParamsButtonStandard;
    LinearLayout.LayoutParams viewParamsButtonScoreValidation;
    LinearLayout.LayoutParams viewParamsButtonReset;
    LinearLayout.LayoutParams viewParamsButtonMatch;


    /*CONSTRUCTOR*/
    public CustomButton(Context activity, LinearLayout myLayout, View.OnClickListener buttonListener){
        this.activity = activity;
        this.myLayout = myLayout;
        this.buttonListener = buttonListener;
    }

    /*GETTER*/

    public int getId(){
        return match.getId();
    }
    /*SETTER*/

    public void setColorBackgroundButtonStandard (int drawable){
        standard.setBackgroundResource(drawable);
    }
    public void setColorBackgroundButtonMatch (int matchId, int drawable){
        Log.i("INFO", "matchId = " + matchId);
        match = myLayout.findViewById(matchId);
        Log.i("INFO", "je récupère le button grace a son id");
        if (match != null) {
            match.setBackgroundResource(drawable);
            Log.i("INFO", "je met le new background");
        }
    }
    public void setColorTextButtonStandard (int color){
        standard.setTextColor(color);
    }
    public void setVisibilityButtonNext(){
        next.setVisibility(View.VISIBLE);
    }
    public void setInvisibilityButtonNext(){
        next.setVisibility(View.INVISIBLE);
    }
    public void setDeactivateValidationScore(){
        scoreValidation.setClickable(false);
        Log.i("INFO", "je rend le btn validation score non clikable");

    }
    public void setDeactivateReset(){
        reset.setClickable(false);
        Log.i("INFO", "je rend le btn reset non clikable");

    }
    public void setActivateValidationScore(){
        scoreValidation.setClickable(true);
        Log.i("INFO", "je rend le btn validation score clikable");

    }
    public void setActivateReset(){
        reset.setEnabled(true);
        Log.i("INFO", "je rend le btn reset clikable");

    }
    public void setColorBackgroundButtonScore (int drawable){
        scoreValidation.setBackgroundResource(drawable);
    }

    public void setColorBackgroundButtonReset (int drawable){
        reset.setBackgroundResource(drawable);
    }

    public void setMarginButtonStandard(int left, int top, int right, int bottom){
        viewParamsButtonStandard.setMargins(left, top, right, bottom);
    }

    /*METHOD*/

    /* Construct all Button that open new activity
     * */
    @SuppressLint("ResourceType")
    public void constructorButtonStandard(String name){

        // Initialisation from Button
        standard = new Button(this.activity);

        // Set up LayoutParams
        viewParamsButtonStandard  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        viewParamsButtonStandard.setMargins(50, 20, 50, 20);
        standard.setLayoutParams(viewParamsButtonStandard);

        // Set up background
        standard.setBackgroundResource(R.drawable.corner_round);

        // Set up font
        komikax = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/komikax.ttf");
        standard.setTypeface(komikax);

        // Set up text
        standard.setText(name);
        standard.setTextColor(Color.DKGRAY);
        standard.setTextColor(R.color.first);

        // Set up Listener
        standard.setOnClickListener(buttonListener);

        // Add view in layout
        myLayout.addView(standard);
    }

    /* Construct Button for validation score
     * */
    @SuppressLint("ResourceType")
    public void constructorButtonScoreValidation(String name){
        // Initialisation from Button
        scoreValidation = new Button(this.activity);

        // Set up LayoutParams
        viewParamsButtonScoreValidation = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                2
        );
        viewParamsButtonScoreValidation.setMargins(0, 0, 50, 0);
        scoreValidation.setLayoutParams(viewParamsButtonScoreValidation);

        // Set up position
        //viewParamsButtonScoreValidation.setMargins(0, 20, 0, 0);
        //scoreValidation.setLayoutParams(viewParamsButtonScoreValidation);

        // Set up background
        scoreValidation.setBackgroundResource(R.drawable.corner_round);

        // Set up font
        komikax = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/komikax.ttf");
        scoreValidation.setTypeface(komikax);

        // Set up text
        scoreValidation.setText(name);
        scoreValidation.setTextColor(R.color.first);

        // Set up Listener
        scoreValidation.setOnClickListener(buttonListener);

        // Add view in layout
        myLayout.addView(scoreValidation);
    }

    /* Construct Button for reset score
     * */
    @SuppressLint("ResourceType")
    public void constructorButtonReset(){

        // Initialisation from Button
        reset = new ImageButton(this.activity);

        // Set up LayoutParams
        viewParamsButtonReset = new LinearLayout.LayoutParams(
                0,
                130,
                1
        );
        reset.setLayoutParams(viewParamsButtonReset);

        // Set icon as image source
        reset.setImageResource(R.drawable.reset_icon);

        // Set up background
        reset.setBackgroundResource(R.drawable.corner_round_invalidation);

        // Disable button
        //reset.setClickable(false);
        reset.setEnabled(false);

        // Set up Listener
        reset.setOnClickListener(buttonListener);

        // Add view in layout
        myLayout.addView(reset);
    }

    /* Construct all Button that open new activity
     * */
    @SuppressLint({"ResourceType", "SetTextI18n"})
    public void constructorButtonNext(){

        // Initialisation from Button
        next = new Button(this.activity);

        // Set up LayoutParams
        viewParamsButtonStandard  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        viewParamsButtonStandard .setMargins(50, 20, 50, 20);
        next.setLayoutParams(viewParamsButtonStandard);

        // Set up background
        next.setBackgroundResource(R.drawable.corner_round_next);

        // Set up font
        komikax = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/komikax.ttf");
        next.setTypeface(komikax);

        // Set up text
        next.setText("Suivant");
        next.setTextColor(R.color.white);

        // Set up Listener
        next.setOnClickListener(buttonListener);

        // Set up visibility
        next.setVisibility(View.INVISIBLE);

        // Add view in layout
        myLayout.addView(next);
    }

    /* Construct Button Match
   Take two attribut for display names players
    * */
    @SuppressLint({"ResourceType", "SetTextI18n"})
    public void constructorButtonMatch(String player1, String player2, int matchId){

        // Initialisation from Button
        match = new Button(this.activity);

        // Unique ID assignment
        match.setId(matchId);

        // Set up LayoutParams
        viewParamsButtonMatch = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        viewParamsButtonMatch .setMargins(50, 20, 50, 20);
        match.setLayoutParams(viewParamsButtonMatch);

        // Set up background
        match.setBackgroundResource(R.drawable.corner_round);

        // Set up font
        komikax = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/komikax.ttf");
        match.setTypeface(komikax);

        // Set up text
        match.setText(player1 + "     VS     " + player2);
        match.setTextColor(R.color.first);

        // Set up Listener
        match.setOnClickListener(buttonListener);

        // Add view in layout
        myLayout.addView(match);
    }
}
