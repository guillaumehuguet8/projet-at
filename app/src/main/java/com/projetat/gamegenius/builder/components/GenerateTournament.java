package com.projetat.gamegenius.builder.components;

import android.util.Log;

public class GenerateTournament {
    /*INITIALISATION*/
    private int numberOfPlayer;

    /*CONSTRUCTOR*/
    public GenerateTournament(int numberOfPlayer){
        this.numberOfPlayer = numberOfPlayer;
    }

    /*GETTER*/

    /*SETTER*/

    /*METHOD*/

    public void constructTournament(int numberOfPlayer){
        Log.i("INFO", "Je construit un tournoi avec : "+numberOfPlayer+"Joueurs/Team");
    }


}
