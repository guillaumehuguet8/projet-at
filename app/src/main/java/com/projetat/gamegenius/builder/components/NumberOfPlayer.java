package com.projetat.gamegenius.builder.components;

import android.util.Log;

public class NumberOfPlayer {
    /*INITIALISATION*/
    private int numberOfMember;

    /*CONSTRUCTOR*/
    public NumberOfPlayer(int numberOfMember){

        this.numberOfMember = numberOfMember;
    }

    /*GETTER*/

    /*SETTER*/

    /*METHOD*/
    public void generateOfInput(int numberOfMember){
        Log.i("INFO", "Je génère " +numberOfMember+ "champs de saisie de nom de Joueur.");
    }

    public void stockOfInput(){
        Log.i("INFO", "Je récupère tous les nom des joueurs. ");

    }
}
