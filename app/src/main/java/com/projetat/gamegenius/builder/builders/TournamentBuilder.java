package com.projetat.gamegenius.builder.builders;

import com.projetat.gamegenius.builder.components.*;
import com.projetat.gamegenius.builder.tournament.*;

public class TournamentBuilder implements Builder {
    /* IMPLEMENTATION */
    private String nameTournament;
    private int numberOfPlayer;
    private GenerateTournament generateTournament;
    private GenerateChampionship generateChampionship;

    /* CONSTRUCTOR */

    public void setNameTournament(String nameTournament) {

        this.nameTournament = nameTournament;
    }


    public void setNumberOfPlayer(int numberOfPlayer) {

        this.numberOfPlayer = numberOfPlayer;
    }

    @Override
    public void setGenerateTournament(GenerateTournament generateTournament) {
        this.generateTournament = generateTournament;
    }

    @Override
    public void setGenerateChampionship(GenerateChampionship generateChampionship) {
        this.generateChampionship = generateChampionship;
    }

    public Tournament getResult(){

        return new Tournament(nameTournament, numberOfPlayer, generateTournament, generateChampionship);
    }
}
