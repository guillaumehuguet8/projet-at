package com.projetat.gamegenius.builder.components.tools;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupFourRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupOneRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupThreeRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupTwoRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateMatch {
    /*INITIALISATION*/
    Context context;
    LinearLayout myLayout;
    int numberOfPlayersGroup;
    static int finalButtonId;
    String name;
    static String keyGroup;
    static List<List<Object>> listRankingGroup;
    ArrayList<String> players;
    public static CustomButton matchButtonGroupOne;
    public static CustomButton matchButtonGroupTwo;
    public static CustomButton matchButtonGroupThree;
    public static CustomButton matchButtonGroupFour;


    /*CONSTRUCTOR*/
    public GenerateMatch(Context context, LinearLayout myLayout, List<List<Object>> listRankingGroup, String keyGroup){
        this.context = context;
        this.myLayout = myLayout;
        GenerateMatch.listRankingGroup = listRankingGroup;
    }

    /*GETTER*/

    /*SETTER*/

    /*METHOD*/

    /*
    *
    * */
    public void generateMatchCalendar(){
        // Récupère le nombre de joueur
        //TODO: je test si ça marche sans le this
        numberOfPlayersGroup = listRankingGroup.size();
        players = new ArrayList<>();

        if (numberOfPlayersGroup % 2 != 0) {
            // Ajouter un joueur fictif si le nombre de joueurs est impair
            players.add("Bye");
            Log.i("INFO", "J'ajoute le joueur bye");
        }

        // Ajout des noms des joueurs dans une liste pour pouvoir organiser les matchs
        //TODO: voir si je dois mettre un < ou <=, se vérifie en comptant les match
        for (int i = 0; i < numberOfPlayersGroup; i++) {
            name = String.valueOf(listRankingGroup.get(i).get(0));
            players.add(name);
        }

        // Générate rounds
        ArrayList<String> rounds = generateRoundRobinSchedule(players, context, myLayout);
        Log.i("INFO", "Je Génère les rounds");

        // Display rounds
        for (String round : rounds) {
            Log.i("ROUNDS", round);
        }
    }

    /*
     *
     * */
    private static ArrayList<String> generateRoundRobinSchedule(ArrayList<String> players, Context context, LinearLayout myLayout){

        // Récupère le nb de playeurs avec la taille de la List car potentiellement +1 au nombre de base pour les chiffres impairs
        int numberPlayer = players.size();
        ArrayList<String> rounds = new ArrayList<>();
        // Gère les id en fonction des group pour que chaque pop up ai son id unique
        if (FragmentSelectTournament.convertInputUserNumberPlayer < 9){
            if (listRankingGroup == FragmentCupGroupOneRanking.listRankingGroupOne){
                finalButtonId = 0;
            }

            else {
                finalButtonId = 6;
            }
        }

        else if (FragmentSelectTournament.convertInputUserNumberPlayer < 13){
            if (listRankingGroup == FragmentCupGroupOneRanking.listRankingGroupOne){
                finalButtonId = 0;
            }

            else {
                finalButtonId = 15;
            }
        }

        else {
            if (listRankingGroup == FragmentCupGroupOneRanking.listRankingGroupOne){
                finalButtonId = 0;
            }

            else if (listRankingGroup == FragmentCupGroupTwoRanking.listRankingGroupTwo){
                finalButtonId = 10;
            }

            else if (listRankingGroup == FragmentCupGroupThreeRanking.listRankingGroupThree){
                finalButtonId = 20;
            }

            else {
                finalButtonId = 30;
            }
        }

        for (int round = 0; round < numberPlayer - 1; round++) {
            // Prépare l'affichage
            StringBuilder roundMatches = new StringBuilder("Round " + (round + 1) + ": ");
            CustomText subtitleRound = new CustomText(context, ("Round " + (round + 1) + ": "), myLayout);
            subtitleRound.constructorSubtitle();


            // Divise le nd de joueur par 2 pour avoir le nb de matchs
            for (int i = 0; i < numberPlayer / 2; i++){
                // Récupère le joueur 1
                String player1 = players.get(i);
                // Récupère le joueur 2
                String player2 = players.get(numberPlayer - 1 - i);
                // Filtrer les joueurs fictifs
                if (!player1.equals("Bye") && !player2.equals("Bye")) {
                    roundMatches.append("[").append(player1).append(" vs ").append(player2).append("] ");

                    int finalButtonId1 = finalButtonId;

                    Log.i("INFO", "keyGroup = " + keyGroup);
                    // Generate button for Group One
                    if (listRankingGroup == FragmentCupGroupOneRanking.listRankingGroupOne) {
                        Log.i("INFO", "je suis dans Group_one");
                        matchButtonGroupOne = new CustomButton(context, myLayout, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup(finalButtonId1, context, player1, player2);
                            }
                        });
                        matchButtonGroupOne.constructorButtonMatch(player1, player2, finalButtonId1);
                    }
                    // Generate button for Group Two
                    else if (listRankingGroup == FragmentCupGroupTwoRanking.listRankingGroupTwo) {
                        matchButtonGroupTwo = new CustomButton(context, myLayout, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup(finalButtonId1, context, player1, player2);
                            }
                        });
                        matchButtonGroupTwo.constructorButtonMatch(player1, player2, finalButtonId1);
                    }
                    // Generate button for Group three
                    else if (listRankingGroup == FragmentCupGroupThreeRanking.listRankingGroupThree) {
                        matchButtonGroupThree = new CustomButton(context, myLayout, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup(finalButtonId1, context, player1, player2);
                            }
                        });
                        matchButtonGroupThree.constructorButtonMatch(player1, player2, finalButtonId1);
                    }
                    // Generate button for Group four
                    else if (listRankingGroup == FragmentCupGroupFourRanking.listRankingGroupFour) {
                        matchButtonGroupFour = new CustomButton(context, myLayout, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showPopup(finalButtonId1, context, player1, player2);
                            }
                        });
                        matchButtonGroupFour.constructorButtonMatch(player1, player2, finalButtonId1);
                    }

                    finalButtonId++;
                }
            }
            // Add round in a List
            rounds.add(roundMatches.toString());
            // Rotation of players except 1
            Collections.rotate(players.subList(1, numberPlayer), 1);
        }
        return rounds;
    }

    /*
     *
     * */
    private static void showPopup(int buttonId, Context context, String playerOne, String playerTwo) {
        Log.i("INFO", "Boutton cliqué match 1");
        final MatchPopupTemplate popupMatch = new MatchPopupTemplate(context, playerOne, playerTwo, buttonId);
        popupMatch.show();
        Log.i("INFO", "Je génère bien la popup");
    }
}
