package com.projetat.gamegenius.builder.components.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityWinner;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.popup.LinearLayoutHorizontal;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;

import java.util.Arrays;
import java.util.Comparator;

public class FragmentChampionshipRanking extends Fragment {

    View rootView;
    LinearLayout myLayout;
    static int rank;
    static String rankString;
    static String name;
    static String points;
    static String score;

    public static CustomButton next;
    public static Object[][] arrayRanking = new Object[FragmentSelectTournament.convertInputUserNumberPlayer][3];

    // Declare a BroadcastReceiver to listen for ranking updates
    private final BroadcastReceiver rankingUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("ranking-updated".equals(intent.getAction())) {
                // Updates the ranking
                updateRanking();
            }
        }
    };

    public FragmentChampionshipRanking() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /* Tab championship Ranking
     * Retrieves the location where the fragment content is to be displayed
     * Create Header Ranking
     * Create Ranking
     * Create Button next
     * */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // MainActivity.toolbar.setTitle("Home");
        rootView = inflater.inflate(R.layout.fragment_championship_ranking, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentChampionshipRanking);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Create Header Ranking
                generateHeaderRanking(getContext(), myLayout);

                // Create Ranking
                generateRanking(getContext(), myLayout);

                // Button next
                // next = new CustomButton(requireContext(), myLayout, buttonListenerNext);
                // next.constructorButtonNext();

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* onStart
    * Registers the BroadcastReceiver to listen to ranking updates
    * */
    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(rankingUpdateReceiver, new IntentFilter("ranking-updated"));
    }

    /* onStop
     * Unregister BroadcastReceiver when no longer needed
     * */
    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(rankingUpdateReceiver);
    }

    /* Generate Header Ranking
     * Create Header Ranking
     * */
    public static void generateHeaderRanking(Context context, LinearLayout myLayout){
        // Create Header Ranking
        LinearLayoutHorizontal headerRanking = new LinearLayoutHorizontal(context, myLayout,"RANG", "NOM", "POINTS", "SCORE");
        //LinearLayoutHorizontal headerRanking = new LinearLayoutHorizontal(context, myLayout,"RANG", "NOM", "SCORE");
        headerRanking.constructorHeaderRanking();
        Log.i("INFO", "headerRanking bien ajouté au layout");
    }

    /* Generate Ranking
    * Sort arrayRanking by point (largest to smallest)
    * If Draw with points, compare score
    * Boucle for : Display ranking
    * Define rank
    * Recover name and score
    * Create a line of ranking with LinearLayoutHorizontal
    * */
    public static void generateRanking(Context context, LinearLayout myLayout){

        // Sort arrayRanking by score
        Arrays.sort(arrayRanking, new Comparator<Object[]>() {
            @Override
            public int compare(Object[] o1, Object[] o2) {
                Integer point1 = (Integer) o1[1]; // Assuming points are at index 1
                Integer point2 = (Integer) o2[1];
                Integer score1 = (Integer) o1[2]; // Assuming scores are at index 2
                Integer score2 = (Integer) o2[2];

                // Compare points first
                int pointComparison = point2.compareTo(point1);

                // If points are equal, compare by score
                if (pointComparison == 0) {
                    return score2.compareTo(score1); // Sort largest to smallest score
                }

                // Otherwise, return the comparison result by points
                return pointComparison;
            }
        });

        // Display ranking
        for(int i = 0; i < FragmentSelectTournament.convertInputUserNumberPlayer; i++){
            rank = i + 1;
            rankString = String.valueOf(rank);
            Log.i("INFO", "rankString = "+rankString);
            name = String.valueOf(arrayRanking[i][0]);
            Log.i("INFO", "name = "+name);
            points = String.valueOf(arrayRanking[i][1]);
            Log.i("INFO", "points = "+points);
            score = String.valueOf(arrayRanking[i][2]);
            Log.i("INFO", "score = "+score);
            //TODO: Remplacer le "0" par le score du joueur
            LinearLayoutHorizontal lineRanking = new LinearLayoutHorizontal(context, myLayout, rankString, name, points, score);
            lineRanking.constructorLineRanking();
        }
    }


    /* Update Ranking
    * Updates the ranking when the BroadcastReceiver receives an update
    * And create next button
    * */
    private void updateRanking() {
        myLayout.removeAllViews(); // Deletes existing views
        generateHeaderRanking(getContext(), myLayout); // Generate header ranking
        generateRanking(getContext(), myLayout); // Generate new views
    }

    /* Listener Button Next
    * Reset the contents of SharedPreferences
    * Calculate the total number of matches to remove all cash bets
    * Remove all cash bets
    * Reset token
    * Open next activity
    * */
    public final View.OnClickListener buttonListenerNext = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {

            // Reset score
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            // Calculate the total number of matches
            int numberOfPlayers = FragmentSelectTournament.convertInputUserNumberPlayer;
            int result = numberOfPlayers * (numberOfPlayers - 1);
            int numberOfMatches = result / 2;

            // Remove all cash bets
            for (int idMatch = 0; idMatch < numberOfMatches; idMatch++){
                editor.remove("scoreOne_" + idMatch);
                editor.remove("scoreTwo_" + idMatch);
                editor.apply();
            }

            // Reset token
            MatchPopupTemplate.token = 0;

            // Open next activity
            Intent activityWinner = new Intent (getContext(), ActivityWinner.class);
            startActivity(activityWinner);
            Log.i("INFO", "J'ouvre la nouvelle activité");
            assert getActivity() != null;
            getActivity().finish();

        }
    };
}

