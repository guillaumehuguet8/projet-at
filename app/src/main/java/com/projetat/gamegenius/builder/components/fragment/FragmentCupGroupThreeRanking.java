package com.projetat.gamegenius.builder.components.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.tools.GenerateArrayRanking;

import java.util.ArrayList;
import java.util.List;

public class FragmentCupGroupThreeRanking extends Fragment {
    View rootView;
    LinearLayout myLayout;
    public static List<List<Object>> listRankingGroupThree = new ArrayList<>();

    // Déclare un BroadcastReceiver pour écouter les mises à jour du classement
    private final BroadcastReceiver rankingUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("ranking-updated".equals(intent.getAction())) {
                // Mettre à jour le classement ici
                updateRanking();
            }
        }
    };

    public FragmentCupGroupThreeRanking() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*
     * Displays the contents of the Group 1
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //MainActivity.toolbar.setTitle("Home");
        rootView = inflater.inflate(R.layout.fragment_cup_group_three_ranking, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentCupGroupThreeRanking);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Affiche le classement de la poule 1
                GenerateArrayRanking rankingGroupThree = new GenerateArrayRanking(getContext(), myLayout, listRankingGroupThree);
                rankingGroupThree.generateHeader();
                Log.i("INFO", "headerRanking bien ajouté au layout");
                rankingGroupThree.generateRanking();
                Log.i("INFO", "Ranking bien ajouté au layout");

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Enregistre le BroadcastReceiver pour écouter les mises à jour du classement
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(rankingUpdateReceiver, new IntentFilter("ranking-updated"));
    }

    @Override
    public void onStop() {
        super.onStop();
        // Désenregistre le BroadcastReceiver lorsqu'il n'est plus nécessaire
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(rankingUpdateReceiver);
    }

    // Méthode pour mettre à jour le classement lorsque le BroadcastReceiver reçoit une mise à jour
    private void updateRanking() {
        myLayout.removeAllViews(); // Supprime les vues existantes
        GenerateArrayRanking rankingGroupThree = new GenerateArrayRanking(getContext(), myLayout, listRankingGroupThree);
        rankingGroupThree.generateHeader();
        rankingGroupThree.generateRanking();
    }
}
