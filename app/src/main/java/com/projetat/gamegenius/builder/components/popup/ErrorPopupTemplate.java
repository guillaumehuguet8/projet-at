package com.projetat.gamegenius.builder.components.popup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;

import java.util.Objects;

public class ErrorPopupTemplate extends Dialog {

    String typeError;
    LinearLayout myLayout;

    String[] error = new String[] {"Vous devez saisir un nom à votre Tournoi pour pouvoir continuer.",
            "Vous devez saisir un nombre de joueurs pour pouvoir continuer.",
            "Pour ce mode de tournoi, le nombre de participants doit être de 2, 4, 8, ou 16.",
            "Le nombre de participants ne peut pas être inférieur à 4 joueurs et ne doit pas dépasser 20 joueurs.",
            "Vous devez remplir tous les champs pour pouvoir continuer.",
            "Dans ce mode jeu, les égualités ne sont pas permises, veuillez départager les deux joueurs.",
            "Vous ne pouvez pas saisir deux fois le même nom de joueur.",
            "Vous devez saisir un type de tournoi pour pouvoir continuer."};

    // CONSTRUCTOR
    public ErrorPopupTemplate(Context context, String typeError){
        super(context, androidx.appcompat.R.style.Base_ThemeOverlay_AppCompat_Dialog_Alert);
        setContentView(R.layout.popup_template_error);
        this.typeError = typeError;
    }

    /* UI for pop up Error
     * Generate UI Design
     * Depending on the type of error, displays a different message
     * */
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        myLayout = (LinearLayout) findViewById(R.id.popupTemplateError);

        //Title
        CustomText titlePopup = new CustomText(getContext(), "ATTENTION", myLayout);
        titlePopup.constructorTitle();

        // TODO: Voir pour la mise en place d'un switch
        if (Objects.equals(this.typeError, "nameTournament")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[0], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "numberPlayer")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[1], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "numberPlayerTournament")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[2], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "numberPlayerHigher")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[3], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "noEntry")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[4], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "Equality")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[5], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "samePlayerName")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[6], myLayout);
            textPopup.constructorText();
        }
        if (Objects.equals(this.typeError, "noTypeOftournament")) {
            //Text
            CustomText textPopup = new CustomText(getContext(), error[7], myLayout);
            textPopup.constructorText();
        }

        //Button
        CustomButton buttonOk = new CustomButton(getContext(), myLayout, buttonListenerOk);
        buttonOk.constructorButtonStandard("OK !");

    }

    /* Listener OK button
     * Just close Popup
     * */
    public final View.OnClickListener buttonListenerOk = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué");
            dismiss();
        }
    };
}
