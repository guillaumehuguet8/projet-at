package com.projetat.gamegenius.builder.components.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.projetat.gamegenius.R;

public class FragmentCupGroupFour extends Fragment {
    View rootView;
    LinearLayout myLayout;

    public FragmentCupGroupFour() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*
     * Displays the contents of the Group 4
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //MainActivity.toolbar.setTitle("Home");
        rootView = inflater.inflate(R.layout.fragment_cup_group_four, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentCupGroupFour);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Ajouter le premier fragment enfant
                if (savedInstanceState == null) {
                    getChildFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainerCupGroupFourRanking, new FragmentCupGroupFourRanking())
                            .commit();

                    // Ajouter le deuxième fragment enfant
                    getChildFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainerCupGroupFourMatch, new FragmentCupGroupFourMatch())
                            .commit();
                }

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }
}
