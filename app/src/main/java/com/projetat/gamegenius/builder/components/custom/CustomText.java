package com.projetat.gamegenius.builder.components.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.projetat.gamegenius.R;

/*
*
* */
@SuppressLint("ViewConstructor")
public class CustomText extends androidx.appcompat.widget.AppCompatTextView {
    /*INITIALISATION*/
    Context activity;
    String text;
    LinearLayout myLayout;
    TextView title;
    TextView title2;
    TextView subtitle;
    TextView allText;
    TextView headerTextRank;
    TextView headerTextRankName;
    TextView textRank;
    TextView textRankName;
    Typeface anton;
    Typeface contrailone;
    Typeface roboto;
    LinearLayout.LayoutParams viewParamsSubtitle;
    LinearLayout.LayoutParams viewParamsTitle;
    LinearLayout.LayoutParams viewParamsHeaderTextRank;
    LinearLayout.LayoutParams viewParamsText;
    LinearLayout.LayoutParams viewParamsHeaderTextRankName;


    /*CONSTRUCTOR*/
    public CustomText(Context activity, String text, LinearLayout myLayout){
        super(activity);
        this.activity = activity;
        this.text = text;
        this.myLayout = myLayout;
    }

    /*GETTER*/

    /*SETTER*/
    public void setMarginTitle(int left, int top, int right, int bottom){
        viewParamsTitle.setMargins(left, top, right, bottom);
    }

    public void setMarginSubtitle(int left, int top, int right, int bottom){
        viewParamsSubtitle.setMargins(left, top, right, bottom);
    }

    public void setMarginText(int left, int top, int right, int bottom){
        viewParamsText.setMargins(left, top, right, bottom);
    }

    public void setColorBackgroundTitle (int drawable){
        title2.setBackgroundResource(drawable);
    }

    public void setBackgroundTextRank (int drawable){
        textRank.setBackgroundResource(drawable);
    }
    public void setBackgroundTextRankName (int drawable){
        textRankName.setBackgroundResource(drawable);
    }

    public void setColorTextRank (int color){
        textRank.setTextColor(color);
    }

    public void setText(String text) {
        this.textRank.setText(text);
    }

    public void setSizeTitle(int size){
        title.setTextSize(size);
    }

    public void setSizeSubtitle(int size){
        subtitle.setTextSize(size);
    }

    /*METHOD*/

    /* Construct all Title
     * */
    public void constructorTitle(){
        // Initialisation from TextView
        title = new TextView(this.activity);

        // Set up LayoutParams
        // TODO: changer les pixel pour le responsive
        viewParamsTitle  = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        viewParamsTitle .setMargins(0, 0, 0, 0);
        title.setLayoutParams(viewParamsTitle);

        // Set up font
        anton = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/anton.ttf");
        title.setTypeface(anton);

        // Set up text
        title.setText(this.text);
        title.setTextSize(25);
        title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(title);
    }

    /* Construct all Subtitle
     * */
    public void constructorSubtitle(){
        // Initialisation from TextView
        subtitle = new TextView(this.activity);

        // Set up LayoutParams
        viewParamsSubtitle = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        // TODO: changer les pixel pour le responsive
        viewParamsSubtitle.setMargins(0, 60, 0, 30);
        subtitle.setLayoutParams(viewParamsSubtitle);

        // Set up font
        anton = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/anton.ttf");
        subtitle.setTypeface(anton);

        // Set up text
        subtitle.setText(this.text);
        subtitle.setTextSize(20);
        subtitle.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(subtitle);
    }

    /* Construct all text
     * */
    public void constructorText(){
        // Initialisation from TextView
        allText = new TextView(this.activity);

        // Set up LayoutParams
        viewParamsText = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, // Largeur
                LinearLayout.LayoutParams.WRAP_CONTENT  // Hauteur
        );

        // Set up position
        // TODO: changer les pixel pour le responsive
        viewParamsText.setMargins(30, 30, 30, 60);
        allText.setLayoutParams(viewParamsText);

        // Set up font
        roboto = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/roboto.ttf");
        allText.setTypeface(roboto);

        // Set up text
        allText.setText(this.text);
        allText.setTextSize(14);
        allText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(allText);
    }

    /* Construct text Header for LinearLayoutHorizontal
     * */
    @SuppressLint("ResourceAsColor")
    public void constructorHeaderTextRank(){
        // Initialisation from TextView
        headerTextRank = new TextView(this.activity);

        // Set up LayoutParams
        viewParamsHeaderTextRank = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        );

        // Set up position
        viewParamsHeaderTextRank.setMargins(0, 50, 0, 0);
        headerTextRank.setLayoutParams(viewParamsHeaderTextRank);

        // Set up font
        roboto = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/roboto.ttf");
        headerTextRank.setTypeface(roboto);

        // Set up background
        headerTextRank.setBackgroundResource(R.drawable.rectangle_header_ranking);

        // Set up text
        headerTextRank.setText(this.text);
        headerTextRank.setTextSize(14);
        headerTextRank.setTextColor(Color.WHITE);
        headerTextRank.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(headerTextRank);
    }

    /* Construct text Header for LinearLayoutHorizontal (Name)
     * */
    @SuppressLint("ResourceAsColor")
    public void constructorHeaderTextRankName(){
        // Initialisation from TextView
        headerTextRankName = new TextView(this.activity);

        // Set up LayoutParams
        viewParamsHeaderTextRankName = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                2
        );

        // Set up position
        // TODO: changer les pixel pour le responsive
        viewParamsHeaderTextRankName.setMargins(0, 50, 0, 0);
        headerTextRankName.setLayoutParams(viewParamsHeaderTextRankName);

        // Set up font
        roboto = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/roboto.ttf");
        headerTextRankName.setTypeface(roboto);

        // Set up background
        headerTextRankName.setBackgroundResource(R.drawable.rectangle_header_ranking);

        // Set up text
        headerTextRankName.setText(this.text);
        headerTextRankName.setTextSize(14);
        headerTextRankName.setTextColor(Color.WHITE);
        headerTextRankName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(headerTextRankName);
    }

    /* Construct text for LinearLayoutHorizontal
     * */
    @SuppressLint("ResourceAsColor")
    public void constructorTextRank(){
        // Initialisation from TextView
        textRank = new TextView(this.activity);

        // Set up LayoutParams
        textRank.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1));

        // Set up position

        // Set up font
        roboto = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/roboto.ttf");
        textRank.setTypeface(roboto);

        // Set up background
        textRank.setBackgroundResource(R.drawable.rectangle_ranking);

        // Set up text
        textRank.setText(this.text);
        textRank.setTextSize(14);
        textRank.setTextColor(R.color.black);
        textRank.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(textRank);
    }

    /* Construct text for LinearLayoutHorizontal (Name)
     * */
    @SuppressLint("ResourceAsColor")
    public void constructorTextRankName(){
        // Initialisation from TextView
        textRankName = new TextView(this.activity);

        // Set up LayoutParams
        textRankName.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                2));

        // Set up position

        // Set up font
        roboto = Typeface.createFromAsset(this.activity.getResources().getAssets(), "fonts/roboto.ttf");
        textRankName.setTypeface(roboto);

        // Set up background
        textRankName.setBackgroundResource(R.drawable.rectangle_ranking);

        // Set up text
        textRankName.setText(this.text);
        textRankName.setTextSize(14);
        textRankName.setTextColor(R.color.black);
        textRankName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        // Add view in layout
        this.myLayout.addView(textRankName);
    }

}
