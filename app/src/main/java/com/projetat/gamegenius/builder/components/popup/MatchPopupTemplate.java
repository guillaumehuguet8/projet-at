package com.projetat.gamegenius.builder.components.popup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityCup;
import com.projetat.gamegenius.builder.components.activity.ActivityTournament;
import com.projetat.gamegenius.builder.components.custom.CustomInputUser;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentChampionshipRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupFourMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupFourRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupOneMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupOneRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupThreeMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupThreeRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupTwoMatch;
import com.projetat.gamegenius.builder.components.fragment.FragmentCupGroupTwoRanking;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.projetat.gamegenius.builder.components.fragment.FragmentTournamentEighth;
import com.projetat.gamegenius.builder.components.fragment.FragmentTournamentFinal;
import com.projetat.gamegenius.builder.components.fragment.FragmentTournamentQuarter;
import com.projetat.gamegenius.builder.components.fragment.FragmentTournamentSemi;
import com.projetat.gamegenius.builder.components.tools.GenerateMatch;
import com.projetat.gamegenius.builder.components.tools.LinearLayoutHorizontalButton;

import java.util.List;
import java.util.Objects;

public class MatchPopupTemplate extends Dialog {

    LinearLayout myLayout;
    String namePlayerOne;
    String namePlayerTwo;
    String scoreOne;
    String scoreTwo;
    String scoreOneSaved;
    String scoreTwoSaved;
    String keyGroup;
    int numberScoreOne;
    int numberScoreTwo;
    int matchId;
    int numberOfMatchCup;

    public static int token;
    public static List<List<Object>> listRankingGroup;
    public static CustomInputUser inputScoreOne;
    public static CustomInputUser inputScoreTwo;

    // Calculate the total number of matches
    int numberOfPlayers = FragmentSelectTournament.convertInputUserNumberPlayer;
    int result = numberOfPlayers * (numberOfPlayers - 1);
    int numberOfMatches = result / 2;


    //Constructor
    @SuppressLint("MissingInflatedId")
    public MatchPopupTemplate(Context context, String namePlayerOne, String namePlayerTwo, int matchId){
        super(context, androidx.appcompat.R.style.Base_ThemeOverlay_AppCompat_Dialog_Alert);
        setContentView(R.layout.popup_template_match);
        this.namePlayerOne = namePlayerOne;
        this.namePlayerTwo = namePlayerTwo;
        this.matchId = matchId;
    }

    /* Popup Match
     * Recovers Layout of popup match
     * Display UI Components
     * Recovers data of input user, if data is different of defValue : ""
     * Else display defValue : ""
     * */
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        myLayout = (LinearLayout) findViewById(R.id.popupTemplateMatch);

        // Generates the display
        generateUiComponents();

        // Load scores
        loadSavedScores();

        // Linear Layout Horizontal Button
        LinearLayoutHorizontalButton layoutButton = new LinearLayoutHorizontalButton(getContext(), myLayout, buttonListenerValidation, buttonListenerReset);
        layoutButton.constructorLayoutHorizontalButton();

        if (!Objects.equals(scoreOneSaved, "") && !Objects.equals(scoreTwoSaved, "")) {
            // Désactive le button validationScore
            LinearLayoutHorizontalButton.buttonValidation.setDeactivateValidationScore();
            LinearLayoutHorizontalButton.buttonValidation.setColorBackgroundButtonScore(R.drawable.corner_round_invalidation);

            // Active le button reset
            LinearLayoutHorizontalButton.buttonReset.setActivateReset();
            LinearLayoutHorizontalButton.buttonReset.setColorBackgroundButtonReset(R.drawable.corner_round);
        }
    }

    /* Generate Ui Components
     * Create titlePopup, namePlayerOne, namePlayerTwo, inputScoreOne, inputScoreTwo, textVs
     * */
    private void generateUiComponents() {

        Log.i("INFO", "scoreOneSaved = " + scoreOneSaved);
        Log.i("INFO", "scoreTwoSaved = " + scoreTwoSaved);
        // Title
        CustomText titlePopup = new CustomText(getContext(), "Match", myLayout);
        titlePopup.constructorTitle();

        // Name Player One
        CustomText namePlayerOne = new CustomText(getContext(), this.namePlayerOne, myLayout);
        namePlayerOne.constructorSubtitle();

        // Edit Score
        inputScoreOne = new CustomInputUser(getContext(), myLayout);
        inputScoreOne.constructorEditNumber("Score :");
        Log.i("INFO", "scoreOneSaved = " + scoreOneSaved);

        // Text
        CustomText textVs = new CustomText(getContext(), "VS", myLayout);
        textVs.constructorText();
        textVs.setMarginText(30,30,30,30);

        // Edit Score
        inputScoreTwo = new CustomInputUser(getContext(), myLayout);
        inputScoreTwo.constructorEditNumber("Score :");
        Log.i("INFO", "scoreTwoSaved = " + scoreTwoSaved);

        // Name Player Two
        CustomText namePlayerTwo = new CustomText(getContext(), this.namePlayerTwo, myLayout);
        namePlayerTwo.constructorSubtitle();
        namePlayerTwo.setMarginSubtitle(0,30,0,60);

        Log.i("INFO", "Tous les composant de l'UI ont bien été initialisé");
    }

    /* Load Saved Scores
     * Load score if any data is saved
     * Recovers saved data for display
     * */
    private void loadSavedScores() {
        // Load score
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
        scoreOneSaved = sharedPreferences.getString("scoreOne_" + matchId, "");
        scoreTwoSaved = sharedPreferences.getString("scoreTwo_" + matchId, "");

        // Recovers saved data
        if (!Objects.equals(scoreOneSaved, "") && !Objects.equals(scoreTwoSaved, "")) {
            Log.i("INFO", "inputScoreOne = " + inputScoreOne);
            Log.i("INFO", "scoreOneSaved = " + scoreOneSaved);
            Log.i("INFO", "inputScoreTwo = " + inputScoreTwo);
            Log.i("INFO", "scoreTwoSaved = " + scoreTwoSaved);
            if (inputScoreOne != null && scoreOneSaved != null) {
                // Recover score one saved
                inputScoreOne.setTextEditNumber(scoreOneSaved);
                // Block input player one
                inputScoreOne.setLockOrUnlockInputUserScore(false);
            }
            if (inputScoreTwo != null && scoreTwoSaved != null) {
                // Recover score two saved
                inputScoreTwo.setTextEditNumber(scoreTwoSaved);
                // Block input player two
                inputScoreTwo.setLockOrUnlockInputUserScore(false);
            }
        }
        Log.i("INFO", "Scores loaded");
    }

    /* Get Index In Array
    * Returns an index if "String target" is equal to a data item in the Array
    * */
    public static int getIndexInArray(Object[][] arrayRanking, String target) {
        for (int i = 0; i < arrayRanking.length; i++) {
            if (arrayRanking[i][0].equals(target)) {
                return i;
            }
        }
        return -1;
    }

    /* Get Index In Array List
     * Returns an index if "String target" is equal to a data item in the List
     * */
    public static int getIndexInArrayList(List<List<Object>> arrayRanking, String target) {
        for (int i = 0; i < arrayRanking.size(); i++) {
            if (arrayRanking.get(i).get(0).equals(target)) {
                return i;
            }
        }
        return -1;
    }

    /* Listener Button validation Score
     * Convert input into String
     * Checks if for empty inputs
     *
     * Save input user
     * Convert input user in int
     * Manage score for different type tournament
     * */
    public final View.OnClickListener buttonListenerValidation = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué validé score");

            // Convert EditText to String
            scoreOne = inputScoreOne.getEditNumber();
            scoreTwo = inputScoreTwo.getEditNumber();

            // Checks if for empty inputs
            if (Objects.equals(scoreOne, "") || Objects.equals(scoreTwo, "")) {
                final ErrorPopupTemplate popupError = new ErrorPopupTemplate(getContext(), "noEntry");
                popupError.show();
            } else {

                Log.i("INFO", "je suis la 3");
                Log.i("INFO", "matchId = "+matchId);

                // Save input
                saveScores();

                // Convert input user in int
                numberScoreOne = Integer.parseInt(scoreOne);
                numberScoreTwo = Integer.parseInt(scoreTwo);

                // Manage score for different type tournament
                if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[0]) || Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[3])) {
                    Log.i("INFO", "je rentre dans le if tournoi");
                    manageTournamentScores();

                } else if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[1])) {
                    manageChampionshipScores();

                } else {
                    manageCupScores();

                }
            }
        }
    };

    /* Listener Button Reset
     * Convert input into String
     * Checks if for empty inputs
     *
     * Save input user
     * Convert input user in int
     * Manage score for different type tournament
     * */
    public final View.OnClickListener buttonListenerReset = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            Log.i("INFO", "Boutton cliqué reset score");

            // Unblock input users
            inputScoreOne.setLockOrUnlockInputUserScore(true);
            inputScoreTwo.setLockOrUnlockInputUserScore(true);

            Log.i("INFO", "Les input sont débloqués");
            // Convert input user in int
            numberScoreOne = Integer.parseInt(scoreOneSaved);
            numberScoreTwo = Integer.parseInt(scoreTwoSaved);
            Log.i("INFO", "numberScoreOne : " +numberScoreOne);
            Log.i("INFO", "numberScoreTwo : " +numberScoreTwo);

            // Remove the token
            token--;


            // Activate the validation score button
            LinearLayoutHorizontalButton.buttonValidation.setActivateValidationScore();
            LinearLayoutHorizontalButton.buttonValidation.setColorBackgroundButtonScore(R.drawable.corner_round);

            // Disable the reset button
            LinearLayoutHorizontalButton.buttonReset.setDeactivateReset();
            LinearLayoutHorizontalButton.buttonReset.setColorBackgroundButtonReset(R.drawable.corner_round_invalidation);

            // Manage reset for type tournament
            if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[0]) || Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[3])) {
                Log.i("INFO", "je rentre dans le if tournoi");
                manageResetTournament();

            }
            // Manage reset for type championship
            else if (Objects.equals(DetailPopupTemplate.keyTypeTournament, FragmentSelectTournament.typeTournament[1])) {
                Log.i("INFO", "je rentre dans le if championship");
                manageResetChampionship();

            }
            // Manage reset for type cup
            else {
                Log.i("INFO", "je rentre dans le else cup");
                manageResetCup();

            }

            // Reset user input of the current match
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("scoreOne_" + matchId);
            editor.remove("scoreTwo_" + matchId);
            editor.apply();

        }
    };

    /* Save Scores
     * Sava score in SharedPreferences
     * */
    private void saveScores() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("scoreOne_" + matchId, scoreOne);
        editor.putString("scoreTwo_" + matchId, scoreTwo);
        editor.apply();
        Log.i("INFO", "Scores sauvegardé");
    }

    /* Manage Tournament Scores
     * Is score is draw, open pop up
     * Add +1 at "token"
     * Else if score one is the biggest, call method stockTournamentQualification() with name player one
     * Else score two is the biggest, call method stockTournamentQualification() with name player two
     * */
    private void manageTournamentScores() {
        // Display pop up error if draw
        if (numberScoreOne == numberScoreTwo) {
            final ErrorPopupTemplate popupErrorEquality = new ErrorPopupTemplate(getContext(), "Equality");
            popupErrorEquality.show();
        }
        // Player 1 win
        else if (numberScoreOne > numberScoreTwo) {
            // Token
            token++;
            Log.i("INFO", "j'ajoute +1 au token'");
            stockTournamentQualification(namePlayerOne);
        }
        // Player 2 win
        else {
            // Token
            token++;
            stockTournamentQualification(namePlayerTwo);
        }
    }

    /* Stock Tournament Qualification
     * Stock "player Name" in Array according to the progress of the tournament
     * Change background button match
     * If all matches have been played, displays the next button
     * Close pop up
     * */
    private void stockTournamentQualification(String playerName) {
        // Stock for Quarter
        if (FragmentSelectTournament.convertInputUserNumberPlayer == 16) {
            ActivityTournament.playerNameQualifiedQuarter[matchId] = playerName;

            // Change background button match
            if (numberScoreOne > numberScoreTwo){
                FragmentTournamentEighth.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
            } else {
                FragmentTournamentEighth.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
            }

            // Display button next if all matches are made
            if (token == 8){
                FragmentTournamentEighth.next.setVisibilityButtonNext();
            }
        }
        // Stock for Semi
        else if (FragmentSelectTournament.convertInputUserNumberPlayer == 8) {
            ActivityTournament.playerNameQualifiedSemi[matchId] = playerName;

            // Change background button match
            if (numberScoreOne > numberScoreTwo){
                FragmentTournamentQuarter.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
            } else {
                FragmentTournamentQuarter.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
            }

            // Display button next if all matches are made
            if (token == 4){
                FragmentTournamentQuarter.next.setVisibilityButtonNext();
            }
        }
        // Stock for Final
        else if (FragmentSelectTournament.convertInputUserNumberPlayer == 4) {
            ActivityTournament.playerNameQualifiedFinal[matchId] = playerName;

            // Change background button match
            if (numberScoreOne > numberScoreTwo){
                FragmentTournamentSemi.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
            } else {
                FragmentTournamentSemi.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
            }

            // Display button next if all matches are made
            if (token == 2){
                FragmentTournamentSemi.next.setVisibilityButtonNext();
            }
        }
        // Stock Winner
        else {
            ActivityTournament.playerNameWinner[matchId] = playerName;

            // Change background button match
            if (numberScoreOne > numberScoreTwo){
                FragmentTournamentFinal.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
            } else {
                FragmentTournamentFinal.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
            }

            if (token == 1){
                Log.i("INFO", "je fais apparaitre le btn next");
                FragmentTournamentFinal.next.setVisibilityButtonNext();
            }
        }
        // Close pop up
        dismiss();
    }

    /* Manage Reset Tournament
     * Reset background for button match
     * Remove button next if display
     * For Eighth, Quarter, Semi, Final
     * */
    private void manageResetTournament(){
        // Reset for Eighth
        if (FragmentSelectTournament.convertInputUserNumberPlayer == 16){
            // Reset background btn match
            FragmentTournamentEighth.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Make the btn next disappear if it appears
            if (token == 7){
                Log.i("INFO", "je fais apparaitre le btn next");
                FragmentTournamentEighth.next.setInvisibilityButtonNext();
            }
        }
        // Reset for Quarter
        else if (FragmentSelectTournament.convertInputUserNumberPlayer == 8) {
            // Reset background btn match
            FragmentTournamentQuarter.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Make the btn next disappear if it appears
            if (token == 3){
                Log.i("INFO", "je fais apparaitre le btn next");
                FragmentTournamentQuarter.next.setInvisibilityButtonNext();
            }
        }
        // Reset for Semi
        else if (FragmentSelectTournament.convertInputUserNumberPlayer == 4) {
            // Reset background btn match
            FragmentTournamentSemi.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Make the btn next disappear if it appears
            if (token == 1){
                Log.i("INFO", "je fais apparaitre le btn next");
                FragmentTournamentSemi.next.setInvisibilityButtonNext();
            }
        }
        // Reset for Final
        else {
            // Reset background btn match
            FragmentTournamentFinal.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Make the btn next disappear if it appears
            if (token == 0){
                Log.i("INFO", "je fais apparaitre le btn next");
                FragmentTournamentFinal.next.setInvisibilityButtonNext();
            }
        }
    }

    /* Manage Championship Scores
     * Add +1 at "token"
     * Checks the match result
     * Calls updateChampionshipRanking() with the player's name, the number of points he has received and score player
     * If all matches have been played, displays the next button
     * Sends a notification to update the ranking
     * Close popup
     * */
    private void manageChampionshipScores() {
        // Token
        token++;

        if (numberScoreOne > numberScoreTwo) {
            updateChampionshipRanking(namePlayerOne, 3, numberScoreOne);
            updateChampionshipRanking(namePlayerTwo, 0, numberScoreTwo);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);

            // Display button next if all matches are made
            if (token == numberOfMatches){
                FragmentChampionshipMatch.next.setVisibilityButtonNext();
            }
        }
        else if (numberScoreOne < numberScoreTwo) {
            updateChampionshipRanking(namePlayerTwo, 3, numberScoreTwo);
            updateChampionshipRanking(namePlayerOne, 0, numberScoreOne);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);

            // Display button next if all matches are made
            if (token == numberOfMatches){
                FragmentChampionshipMatch.next.setVisibilityButtonNext();
            }
        }
        else {
            updateChampionshipRanking(namePlayerOne, 1, numberScoreOne);
            updateChampionshipRanking(namePlayerTwo, 1, numberScoreTwo);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_invalidation);

            // Display button next if all matches are made
            if (token == numberOfMatches){
                FragmentChampionshipMatch.next.setVisibilityButtonNext();
            }
        }
        notifyRankingUpdate();
        dismiss();


    }

    /* Update Championship Ranking
     * Recovers player index with getIndexInArray()
     * Recovers player's point and score
     * Adds points and score to player's
     * Stock player's new score and new score
     * */
    private void updateChampionshipRanking(String playerName, int points, int score) {
        // Add Points in Ranking
        int index = getIndexInArray(FragmentChampionshipRanking.arrayRanking, playerName);
        int oldPoints = (int) FragmentChampionshipRanking.arrayRanking[index][1];
        int newPoints = oldPoints + points;
        FragmentChampionshipRanking.arrayRanking[index][1] = newPoints;
        Log.i("INFO", playerName + " new points: " + newPoints);

        // Add Score in Ranking
        int oldScore = (int) FragmentChampionshipRanking.arrayRanking[index][2];
        Log.i("INFO", " oldScore: " + oldScore);
        int newScore = oldScore + score;
        Log.i("INFO", " newScore: " + newScore);
        FragmentChampionshipRanking.arrayRanking[index][2] = newScore;
        Log.i("INFO", playerName + " new score: " + newScore);
    }

    /* Manage Reset Championship
     * Checks old match result
     * Calls updateChampionshipRanking() with the player's name, the number of points he removed and score
     * Remove button next if display
     * Sends a notification to update the ranking
     * */
    private void manageResetChampionship(){
        if (numberScoreOne > numberScoreTwo) {
            updateChampionshipRanking(namePlayerOne, -3, -numberScoreOne);
            updateChampionshipRanking(namePlayerTwo, 0, -numberScoreTwo);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Display button next if all matches are made
            if (token == numberOfMatches - 1){
                FragmentChampionshipMatch.next.setInvisibilityButtonNext();
            }
        }

        else if (numberScoreOne < numberScoreTwo) {
            updateChampionshipRanking(namePlayerTwo, -3, -numberScoreTwo);
            updateChampionshipRanking(namePlayerOne, 0, -numberScoreOne);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Display button next if all matches are made
            if (token == numberOfMatches - 1){
                FragmentChampionshipMatch.next.setInvisibilityButtonNext();
            }
        }

        else {
            updateChampionshipRanking(namePlayerOne, -1, -numberScoreOne);
            updateChampionshipRanking(namePlayerTwo, -1, -numberScoreTwo);

            // Change background button match
            FragmentChampionshipMatch.matchButton.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);

            // Display button next if all matches are made
            if (token == numberOfMatches - 1){
                FragmentChampionshipMatch.next.setInvisibilityButtonNext();
            }
        }

        notifyRankingUpdate();
    }

    /* Manage Cup Scores
     * Retrieves the Group key to retrieve the correct List
     * Calls updateChampionshipRanking() with the player's name, the number of points and score of player he has received
     * Sends a notification to update the ranking
     * Close pop up
     * */
    private void manageCupScores() {
        // Retrieves the Group key
        keyGroup = ActivityCup.keyGroup;
        Log.i("INFO", " keyGroup: " + keyGroup);

        // Retrieve the correct List
        if (Objects.equals(keyGroup, "Group_one")) {
            Log.i("INFO", " listRankingGroupOne: " + FragmentCupGroupOneRanking.listRankingGroupOne);
            listRankingGroup = FragmentCupGroupOneRanking.listRankingGroupOne;
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            Log.i("INFO", "  listRankingGroupTwo:  " + FragmentCupGroupTwoRanking.listRankingGroupTwo);
            listRankingGroup = FragmentCupGroupTwoRanking.listRankingGroupTwo;
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            Log.i("INFO", "  listRankingGroupThree:  " + FragmentCupGroupThreeRanking.listRankingGroupThree);
            listRankingGroup = FragmentCupGroupThreeRanking.listRankingGroupThree;
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            Log.i("INFO", "  listRankingGroupFour:  " + FragmentCupGroupFourRanking.listRankingGroupFour);
            listRankingGroup = FragmentCupGroupFourRanking.listRankingGroupFour;
        }

        // Token
        token++;

        // Calculate number of match
        numberOfMatchCup = calculateNumberOfMatchCup();

        // Checks the match result
        if (numberScoreOne > numberScoreTwo) {
            updateCupRanking(namePlayerOne, 3, numberScoreOne);
            updateCupRanking(namePlayerTwo, 0, numberScoreTwo);
            Log.i("INFO", " keyGroup: " + keyGroup);
            updateBackgroundMatchButtonWinLeft(keyGroup);
            if (token == numberOfMatchCup){
                Log.i("INFO", "Je suis dans if win j1");
                displaysNextButton();
            }
        }
        else if (numberScoreOne < numberScoreTwo) {
            updateCupRanking(namePlayerTwo, 3, numberScoreTwo);
            updateCupRanking(namePlayerOne, 0, numberScoreOne);
            Log.i("INFO", " keyGroup: " + keyGroup);
            updateBackgroundMatchButtonWinRight(keyGroup);
            if (token == numberOfMatchCup){
                Log.i("INFO", "Je suis dans le if win j2");
                displaysNextButton();
            }
        }
        else {
            updateCupRanking(namePlayerOne, 1, numberScoreOne);
            updateCupRanking(namePlayerTwo, 1, numberScoreTwo);
            Log.i("INFO", " keyGroup: " + keyGroup);
            updateBackgroundMatchButtonDraw(keyGroup);
            if (token == numberOfMatchCup){
                Log.i("INFO", "Je suis dans le if draw");
                displaysNextButton();
            }
        }

        notifyRankingUpdate();
        dismiss();
    }

    /* Manage Reset Cup
     * Recover key group
     * Recover good list with key group
     * Calcul number of match of cup
     * Calls updateChampionshipRanking() with the player's name, the number of points and score of player he removed
     * Remove button next if display
     * Sends a notification to update the ranking
     * */
    private void manageResetCup(){
        // Retrieves the Group key
        keyGroup = ActivityCup.keyGroup;
        Log.i("INFO", " keyGroup: " + keyGroup);

        // Retrieve the correct List
        if (Objects.equals(keyGroup, "Group_one")) {
            Log.i("INFO", " listRankingGroupOne: " + FragmentCupGroupOneRanking.listRankingGroupOne);
            listRankingGroup = FragmentCupGroupOneRanking.listRankingGroupOne;
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            Log.i("INFO", "  listRankingGroupTwo:  " + FragmentCupGroupTwoRanking.listRankingGroupTwo);
            listRankingGroup = FragmentCupGroupTwoRanking.listRankingGroupTwo;
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            Log.i("INFO", "  listRankingGroupThree:  " + FragmentCupGroupThreeRanking.listRankingGroupThree);
            listRankingGroup = FragmentCupGroupThreeRanking.listRankingGroupThree;
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            Log.i("INFO", "  listRankingGroupFour:  " + FragmentCupGroupFourRanking.listRankingGroupFour);
            listRankingGroup = FragmentCupGroupFourRanking.listRankingGroupFour;
        }

        // Calculate number of match
        numberOfMatchCup = calculateNumberOfMatchCup();

        // Change the match result
        if (numberScoreOne > numberScoreTwo) {
            updateCupRanking(namePlayerOne, -3, -numberScoreOne);
            updateCupRanking(namePlayerTwo, 0, -numberScoreTwo);
            Log.i("INFO", " keyGroup: " + keyGroup);
            resetBackgroundButtonMatchCup(keyGroup);
            if (token == numberOfMatchCup - 1){
                Log.i("INFO", "Je suis dans if win j1");
                resetDisplaysNextButton();
            }
        }
        else if (numberScoreOne < numberScoreTwo) {
            updateCupRanking(namePlayerTwo, -3, -numberScoreTwo);
            updateCupRanking(namePlayerOne, 0, -numberScoreOne);
            Log.i("INFO", " keyGroup: " + keyGroup);
            resetBackgroundButtonMatchCup(keyGroup);
            if (token == numberOfMatchCup - 1){
                Log.i("INFO", "Je suis dans le if win j2");
                resetDisplaysNextButton();
            }
        }
        else {
            updateCupRanking(namePlayerOne, -1, -numberScoreOne);
            updateCupRanking(namePlayerTwo, -1, -numberScoreTwo);
            Log.i("INFO", " keyGroup: " + keyGroup);
            resetBackgroundButtonMatchCup(keyGroup);
            if (token == numberOfMatchCup - 1){
                Log.i("INFO", "Je suis dans le if draw");
                resetDisplaysNextButton();
            }
        }

        notifyRankingUpdate();
    }

    /* Calculate Number Of Match Cup
     * Calculate number of match by group
     * Added all match between them
     * return number of match for cup
     * */
    public int calculateNumberOfMatchCup(){

        // Récupère le nombre de joueur par groupe
        int playersGroupOne = FragmentCupGroupOneRanking.listRankingGroupOne.size();
        Log.i("INFO", "  playersGroupOne:  " + playersGroupOne);
        int playersGroupTwo = FragmentCupGroupTwoRanking.listRankingGroupTwo.size();
        Log.i("INFO", "  playersGroupTwo:  " + playersGroupTwo);
        int playersGroupThree = FragmentCupGroupThreeRanking.listRankingGroupThree.size();
        Log.i("INFO", "  playersGroupThree:  " + playersGroupThree);
        int playersGroupFour = FragmentCupGroupFourRanking.listRankingGroupFour.size();
        Log.i("INFO", "  playersGroupFour:  " + playersGroupFour);

        // Calcul du nombre de match par groupe
        int matchesGroupOne = calculateNumberOfMatch(playersGroupOne);
        Log.i("INFO", "  matchesGroupOne:  " + matchesGroupOne);
        int matchesGroupTwo = calculateNumberOfMatch(playersGroupTwo);
        Log.i("INFO", "  matchesGroupTwo:  " + matchesGroupTwo);
        int matchesGroupThree = calculateNumberOfMatch(playersGroupThree);
        Log.i("INFO", "  matchesGroupThree:  " + matchesGroupThree);
        int matchesGroupFour = calculateNumberOfMatch(playersGroupFour);
        Log.i("INFO", "  matchesGroupFour:  " + matchesGroupFour);

        // Calcul du nombre de match total de la Cup
        int numberOfMatchCup = matchesGroupOne + matchesGroupTwo + matchesGroupThree + matchesGroupFour;
        Log.i("INFO", "  numberOfMatchCup:  " + numberOfMatchCup);

        return numberOfMatchCup;
    }

    /* Calculate Number Of Match
     * For one group for cup
     * Return number of match for one group
     * */
    public int calculateNumberOfMatch(int numberOfPlayers){

        int result = numberOfPlayers * (numberOfPlayers - 1);
        int numberOfMatches = result / 2;

        return numberOfMatches;
    }

    /* Update Cup Ranking
     * Recovers player index with getIndexInArray()
     * Recovers player's points
     * Adds points awarded to player's points
     * Stock player's new points
     * Recovers player's score
     * Adds points awarded to player's score
     * Stock player's new score
     * */
    private void updateCupRanking(String playerName, int points, int score) {
        Log.i("INFO", "  listRankingGroup:  " + listRankingGroup);
        int index = getIndexInArrayList(listRankingGroup, playerName);
        int oldPoints = (int) listRankingGroup.get(index).get(1);
        int newPoints = oldPoints + points;
        listRankingGroup.get(index).set(1, newPoints);
        Log.i("INFO", playerName + " new score: " + newPoints);

        // Add Score in Ranking
        int oldScore = (int) listRankingGroup.get(index).get(2);
        Log.i("INFO", " oldScore: " + oldScore);
        int newScore = oldScore + score;
        Log.i("INFO", " newScore: " + newScore);
        listRankingGroup.get(index).set(2, newScore);
        Log.i("INFO", playerName + " new score: " + newScore);
    }

    /* Update Background Match Button Win Left
    * Change background match button
    * If player left win
    * In function of group
    * */
    private void updateBackgroundMatchButtonWinLeft(String keyGroup){
        Log.i("INFO", " keyGroup: " + keyGroup);
        //
        if (Objects.equals(keyGroup, "Group_one")) {
            // Change background button match
            GenerateMatch.matchButtonGroupOne.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
            Log.i("INFO", "  Group_one  ");
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            // Change background button match
            GenerateMatch.matchButtonGroupTwo.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            // Change background button match
            GenerateMatch.matchButtonGroupThree.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            // Change background button match
            GenerateMatch.matchButtonGroupFour.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_left);
        }
    }

    /* Update Background Match Button Win Right
     * Change background match button
     * If player right win
     * In function of group
     * */
    private void updateBackgroundMatchButtonWinRight(String keyGroup){
        //
        if (Objects.equals(keyGroup, "Group_one")) {
            // Change background button match
            GenerateMatch.matchButtonGroupOne.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
            Log.i("INFO", "  Group_one  ");
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            // Change background button match
            GenerateMatch.matchButtonGroupTwo.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            // Change background button match
            GenerateMatch.matchButtonGroupThree.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            // Change background button match
            GenerateMatch.matchButtonGroupFour.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_winner_right);
        }
    }

    /* Update Background Match Button Draw
     * Change background match button
     * If players right and left are draw
     * In function of group
     * */
    private void updateBackgroundMatchButtonDraw(String keyGroup){
        //
        if (Objects.equals(keyGroup, "Group_one")) {
            // Change background button match
            GenerateMatch.matchButtonGroupOne.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_invalidation);
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            // Change background button match
            GenerateMatch.matchButtonGroupTwo.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_invalidation);
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            // Change background button match
            GenerateMatch.matchButtonGroupThree.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_invalidation);
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            // Change background button match
            GenerateMatch.matchButtonGroupFour.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round_invalidation);
        }
    }

    /* Reset Background Button Match Cup
     * Reset background match button
     * In function of group
     * */
    private void resetBackgroundButtonMatchCup(String keyGroup){
        Log.i("INFO", " keyGroup: " + keyGroup);
        //
        if (Objects.equals(keyGroup, "Group_one")) {
            // Change background button match
            GenerateMatch.matchButtonGroupOne.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);
            Log.i("INFO", "  Group_one  ");
        }
        else if (Objects.equals(keyGroup, "Group_two")) {
            // Change background button match
            GenerateMatch.matchButtonGroupTwo.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);
        }
        else if (Objects.equals(keyGroup, "Group_three")) {
            // Change background button match
            GenerateMatch.matchButtonGroupThree.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);
        }
        else if (Objects.equals(keyGroup, "Group_four")) {
            // Change background button match
            GenerateMatch.matchButtonGroupFour.setColorBackgroundButtonMatch(matchId, R.drawable.corner_round);
        }
    }

    /* Displays Next Button
     * Display next button for all group
     * */
    private void displaysNextButton(){
        FragmentCupGroupOneMatch.next.setVisibilityButtonNext();
        Log.i("INFO", "Next One OK");
        FragmentCupGroupTwoMatch.next.setVisibilityButtonNext();
        Log.i("INFO", "Next Two OK");

        if (FragmentSelectTournament.convertInputUserNumberPlayer > 12){
            FragmentCupGroupThreeMatch.next.setVisibilityButtonNext();
            Log.i("INFO", "Next Three OK");
            FragmentCupGroupFourMatch.next.setVisibilityButtonNext();
            Log.i("INFO", "Next Four OK");
        }

    }

    /* Reset Displays Next Button
     * Remove next button for all group
     * */
    private void resetDisplaysNextButton(){
        FragmentCupGroupOneMatch.next.setInvisibilityButtonNext();
        Log.i("INFO", "Next One OK");
        FragmentCupGroupTwoMatch.next.setInvisibilityButtonNext();
        Log.i("INFO", "Next Two OK");

        if (FragmentSelectTournament.convertInputUserNumberPlayer > 12){
            FragmentCupGroupThreeMatch.next.setInvisibilityButtonNext();
            Log.i("INFO", "Next Three OK");
            FragmentCupGroupFourMatch.next.setInvisibilityButtonNext();
            Log.i("INFO", "Next Four OK");
        }
    }

    /* Notify Ranking Update
    * Sends a notification "ranking-updated" with LocalBroadcastManager
    * */
    private void notifyRankingUpdate() {
        Intent intent = new Intent("ranking-updated");
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }
}
