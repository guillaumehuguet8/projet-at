package com.projetat.gamegenius.builder.components.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityWinner;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.custom.CustomText;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;

import java.util.ArrayList;
import java.util.Collections;


public class FragmentChampionshipMatch extends Fragment {

    View rootView;
    LinearLayout myLayout;
    public static ArrayList<String> players;
    int numberOfPlayers;
    String name;

    public static CustomButton next;
    public static CustomButton matchButton;

    public FragmentChampionshipMatch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /* Tab Championship Match
    * Retrieves the location where the fragment content is to be displayed
    * Retrieve the number of players
    * Check if the number of players is odd, if so add a dummy player
    * Add players' names to a list to organize matches
    * Generate rounds
    * Create Button next
    * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //MainActivity.toolbar.setTitle("Home");
        rootView = inflater.inflate(R.layout.fragment_championship_match, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentChampionshipMatch);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Retrieve the number of players
                numberOfPlayers = FragmentSelectTournament.convertInputUserNumberPlayer;

                // Add players' names to a list to organize matches
                players = new ArrayList<>();
                for (int i = 0; i < numberOfPlayers; i++) {
                    name = String.valueOf(FragmentChampionshipRanking.arrayRanking[i][0]);
                    players.add(name);
                }

                // Check if the number of players is odd
                if (numberOfPlayers % 2 != 0) {
                    // Add a dummy player
                    players.add("Bye");
                }

                // Generate rounds
                ArrayList<String> rounds = generateRoundRobinSchedule(players, getContext(), myLayout);

                // Affiche les rounds dans la console
                /*for (String round : rounds) {
                    Log.i("ROUNDS", round);
                }*/

                // Button Next
                next = new CustomButton(requireContext(), myLayout, buttonListenerNext);
                next.constructorButtonNext();

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Listener Button Next
     * Reset the contents of SharedPreferences
     * Calculate the total number of matches to remove all cash bets
     * Remove all cash bets
     * Reset token
     * Open next activity
     * */
    public final View.OnClickListener buttonListenerNext = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {

            // Reset score
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            // Calculate the total number of matches
            int numberOfPlayers = FragmentSelectTournament.convertInputUserNumberPlayer;
            int result = numberOfPlayers * (numberOfPlayers - 1);
            int numberOfMatches = result / 2;

            // Remove all cash bets
            for (int idMatch = 0; idMatch < numberOfMatches; idMatch++){
                editor.remove("scoreOne_" + idMatch);
                editor.remove("scoreTwo_" + idMatch);
                editor.apply();
            }

            // Reset token
            MatchPopupTemplate.token = 0;

            // Open next activity
            Intent activityWinner = new Intent (getContext(), ActivityWinner.class);
            startActivity(activityWinner);
            Log.i("INFO", "J'ouvre la nouvelle activité");
            assert getActivity() != null;
            getActivity().finish();

        }
    };

    /* Generate Round Robin Schedule
    * Retrieves the number of players with the size of the List
    * Add an ArrayList to store rounds
    * Add a unique id for pop-ups
    * 1st boucle for : iterates through the rounds
    * Add subtitle for display
    * 2nd boucle for : Divide the number of players by 2 to obtain the number of matches
    * Recover player 1 and 2
    * Filter fictitious players
    * Construct Button Match with Listener
    * Rotation of players except 1 so that all players can play with each other
    * */
    public static ArrayList<String> generateRoundRobinSchedule(ArrayList<String> players, Context context, LinearLayout myLayout){

        // Retrieves the number of players
        int numberPlayer = players.size();
        ArrayList<String> rounds = new ArrayList<>();
        int finalButtonId = 0;

        for (int round = 0; round < numberPlayer - 1; round++) {
            // Add subtitle for display
            //StringBuilder roundMatches = new StringBuilder("Round " + (round + 1) + ": ");
            CustomText subtitleRound = new CustomText(context, ("Round " + (round + 1) + ": "), myLayout);
            subtitleRound.constructorSubtitle();


            // Divide the number of players by 2 to obtain the number of matches
            for (int i = 0; i < numberPlayer / 2; i++){
                // Recover player 1
                String player1 = players.get(i);
                // Recover player 2
                String player2 = players.get(numberPlayer - 1 - i);
                // Filter fictitious players
                if (!player1.equals("Bye") && !player2.equals("Bye")) {

                    int finalButtonId1 = finalButtonId;
                    matchButton = new CustomButton(context, myLayout, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPopup(context, player1, player2, finalButtonId1);
                        }
                    });
                    matchButton.constructorButtonMatch(player1, player2, finalButtonId1);
                    finalButtonId++;

                }
            }
            // Rotation of players except 1
            Collections.rotate(players.subList(1, numberPlayer), 1);
        }

        return rounds;
    }

    /* Show Popup
     * Open the match Pop up
     * */
    public static void showPopup(Context context, String playerOne, String playerTwo, int buttonId) {
        Log.i("INFO", "Boutton cliqué match 1");
        final MatchPopupTemplate popupMatch = new MatchPopupTemplate(context, playerOne, playerTwo, buttonId);
        popupMatch.show();
        Log.i("INFO", "Je génère bien la popup");
    }
}