package com.projetat.gamegenius.builder.components.tools;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;
import com.projetat.gamegenius.builder.components.popup.LinearLayoutHorizontal;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GenerateArrayRanking {
    /*INITIALISATION*/
    Context context;
    LinearLayout myLayout;
    List<List<Object>> listRankingGroup;

    /*CONSTRUCTOR*/
    public GenerateArrayRanking(Context context, LinearLayout myLayout, List<List<Object>> listRankingGroup){
        this.context = context;
        this.myLayout = myLayout;
        this.listRankingGroup = listRankingGroup;
    }

    /*GETTER*/

    /*SETTER*/

    /*METHOD*/


    /* Generate Header Ranking
     * Call constructor header ranking for display
     * */
    public void generateHeader(){
        LinearLayoutHorizontal headerRanking = new LinearLayoutHorizontal(this.context, this.myLayout,"RANG", "NOM", "POINTS", "SCORE");
        headerRanking.constructorHeaderRanking();
    }

    /* Generate Ranking
     * Sort arrayRanking by score (largest to smallest)
     * Display ranking
     * Highlight qualified players
     */
    public void generateRanking(){
        int rank;
        String rankString;
        String name;
        String points;
        String score;

        // Sort arrayRanking by score (largest to smallest)
        Collections.sort(this.listRankingGroup, new Comparator<List<Object>>() {
            @Override
            public int compare(List<Object> scorePlayer1, List<Object> scorePlayer2) {
                // Extracting points (cast as Integer)
                int points1 = (int) scorePlayer1.get(1);
                int points2 = (int) scorePlayer2.get(1);

                // Extraction of scores (cast as Integer), assuming the score is at index 2
                int score1 = (int) scorePlayer1.get(2);
                int score2 = (int) scorePlayer2.get(2);

                // Point comparison first
                int pointComparison = Integer.compare(points2, points1);

                // If points are equal, compare scores
                if (pointComparison == 0) {
                    return Integer.compare(score2, score1); // Sort descending on scores
                }

                // Otherwise, return the point-based comparison
                return pointComparison;
            }
        });

        // Display ranking
        Log.i("INFO", "listRankingGroup.size() = "+this.listRankingGroup.size());
        for(int i = 0; i < this.listRankingGroup.size(); i++){
            rank = i + 1;
            rankString = String.valueOf(rank);
            Log.i("INFO", "rankString = "+rankString);
            name = String.valueOf(this.listRankingGroup.get(i).get(0));
            Log.i("INFO", "name = "+name);
            points = String.valueOf(this.listRankingGroup.get(i).get(1));
            Log.i("INFO", "points = "+points);
            score = String.valueOf(this.listRankingGroup.get(i).get(2));
            Log.i("INFO", "points = "+score);
            //TODO: Remplacer le "0" par le score du joueur
            LinearLayoutHorizontal lineRanking = new LinearLayoutHorizontal(this.context, this.myLayout, rankString, name, points, score);
            lineRanking.constructorLineRanking();

            // If the number of players is less than or equal to 8 you highlight 2, otherwise you highlight 4
            if (FragmentSelectTournament.convertInputUserNumberPlayer <= 8 || FragmentSelectTournament.convertInputUserNumberPlayer >= 13){
                if (rank == 1 || rank == 2){
                    lineRanking.setBackgroundLineRanking(R.drawable.rectangle_ranking_qualify);
                    Log.i("INFO", "Design qualify OK");
                }
            } else {
                if (rank == 1 || rank == 2 || rank == 3 || rank == 4){
                    lineRanking.setBackgroundLineRanking(R.drawable.rectangle_ranking_qualify);
                    Log.i("INFO", "Design qualify OK");
                }
            }
        }
    }

}
