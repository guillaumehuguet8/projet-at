package com.projetat.gamegenius.builder.components.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.activity.ActivityCup;
import com.projetat.gamegenius.builder.components.activity.ActivityTournament;
import com.projetat.gamegenius.builder.components.custom.CustomButton;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;
import com.projetat.gamegenius.builder.components.popup.MatchPopupTemplate;
import com.projetat.gamegenius.builder.components.tools.GenerateMatch;

public class FragmentCupGroupTwoMatch extends Fragment {
    View rootView;
    LinearLayout myLayout;

    public static CustomButton next;
    public FragmentCupGroupTwoMatch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*
     * Displays the contents of the Group 2
     * */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //MainActivity.toolbar.setTitle("Home");
        rootView = inflater.inflate(R.layout.fragment_cup_group_two_match, container, false);
        //navController = Navigation.findNavController(rootView);
        if (rootView != null){
            Log.i("INFO", "Le layout a été inflaté avec succès");
            myLayout = rootView.findViewById(R.id.fragmentCupGroupTwoMatch);

            if (myLayout != null){
                Log.i("INFO", "Le LinearLayout a été trouvé avec succès");

                // Display match Group one
                GenerateMatch matchGroupTwo = new GenerateMatch(getContext(), myLayout, FragmentCupGroupTwoRanking.listRankingGroupTwo, ActivityCup.keyGroup);
                matchGroupTwo.generateMatchCalendar();

                // Button Next
                next = new CustomButton(requireContext(), myLayout, buttonListenerNext);
                next.constructorButtonNext();

                return rootView;
            } else {
                Log.i("INFO", "myLayout est null");
                return null;
            }
        } else {
            Log.i("INFO", "rootView est null");
            return null;
        }
    }

    /* Button Listener Next
     * Call méthod removeScoreMatchPopupCup
     * Call méthod playerQualify
     * Reset tokken
     * Open activity Tournament
     * And close activity Cup
     * */
    public final View.OnClickListener buttonListenerNext = new View.OnClickListener(){
        // TODO: A tester (si c'est non null)
        @Override
        public void onClick(View v) {

            removeScoreMatchPopupCup();

            playerQualify();

            // Reset token
            MatchPopupTemplate.token = 0;

            // Open activity Tournament
            Intent activityTournament = new Intent (getContext(), ActivityTournament.class);
            startActivity(activityTournament);
            Log.i("INFO", "J'ouvre la nouvelle activité");
            assert getActivity() != null;
            getActivity().finish();

        }

        /* Remove Score Match PopupCup
         * Initialize SharedPreferences
         * If number of player < 9
         * Remove Score in match pop up with ID 0 -> 11
         * If number of player < 13
         * Remove Score in match pop up with ID 0 -> 29
         * If number of player >= 13
         * Remove Score in match pop up with ID 0 -> 39
         * */
        public void removeScoreMatchPopupCup(){

            // Reset score
            SharedPreferences sharedPreferences = getContext().getSharedPreferences("match_scores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (FragmentSelectTournament.convertInputUserNumberPlayer < 9){
                // Remove all match Group 1 and 2
                for(int idMatch = 0; idMatch < 12; idMatch++){
                    editor.remove("scoreOne_" + idMatch);
                    editor.remove("scoreTwo_" + idMatch);
                    editor.apply();
                }
            }

            else if (FragmentSelectTournament.convertInputUserNumberPlayer < 13){
                // Remove all match Group 1 and 2
                for(int idMatch = 0; idMatch < 30; idMatch++){
                    editor.remove("scoreOne_" + idMatch);
                    editor.remove("scoreTwo_" + idMatch);
                    editor.apply();
                }
            }

            else {
                // Remove all match Group 1, 2, 3 and 4
                for(int idMatch = 0; idMatch < 40; idMatch++){
                    editor.remove("scoreOne_" + idMatch);
                    editor.remove("scoreTwo_" + idMatch);
                    editor.apply();
                }
            }
        }

        /* Player Qualify
         * If number of player < 9
         * Stock name player ID 0 and 1 (these are the qualified) Lists Groups to Array Semi
         * Changing the number of players for adaptation with ActivityTournament
         * If number of player < 13
         * Stock name player ID 0, 1, 2 and 3 (these are the qualified) Lists Groups to Array Quarter
         * Changing the number of players for adaptation with ActivityTournament
         * If number of player >= 13
         * Stock name player ID 0 and 1 (these are the qualified) Lists Groups to Array Quarter
         * Changing the number of players for adaptation with ActivityTournament
         * Change the key type of tournament
         * */
        public void playerQualify(){
            if (FragmentSelectTournament.convertInputUserNumberPlayer < 9){
                // 1er G1 vs 2eme G2
                ActivityTournament.playerNameQualifiedSemi[0] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(0).get(0));
                ActivityTournament.playerNameQualifiedSemi[1] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(1).get(0));
                // 1er G2 vs 2eme G1
                ActivityTournament.playerNameQualifiedSemi[2] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(0).get(0));
                ActivityTournament.playerNameQualifiedSemi[3] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(1).get(0));

                // Changing the number of players for adaptation with ActivityTournament
                FragmentSelectTournament.convertInputUserNumberPlayer = 4;

            }

            else if (FragmentSelectTournament.convertInputUserNumberPlayer < 13) {
                // 1er G1 vs 3eme G2
                ActivityTournament.playerNameQualifiedQuarter[0] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[1] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(2).get(0));
                // 1er G2 vs 3eme G1
                ActivityTournament.playerNameQualifiedQuarter[2] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[3] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(2).get(0));
                // 2eme G1 vs 4eme G2
                ActivityTournament.playerNameQualifiedQuarter[4] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(1).get(0));
                ActivityTournament.playerNameQualifiedQuarter[5] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(3).get(0));
                // 2eme G2 vs 4eme G1
                ActivityTournament.playerNameQualifiedQuarter[6] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(1).get(0));
                ActivityTournament.playerNameQualifiedQuarter[7] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(3).get(0));

                // Changing the number of players for adaptation with ActivityTournament
                FragmentSelectTournament.convertInputUserNumberPlayer = 8;
            }

            else {
                // 1er G1 vs 2eme G2
                ActivityTournament.playerNameQualifiedQuarter[0] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[1] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(1).get(0));
                // 1er G2 vs 2eme G1
                ActivityTournament.playerNameQualifiedQuarter[2] = String.valueOf(FragmentCupGroupTwoRanking.listRankingGroupTwo.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[3] = String.valueOf(FragmentCupGroupOneRanking.listRankingGroupOne.get(1).get(0));
                // 1er G3 vs 2eme G4
                ActivityTournament.playerNameQualifiedQuarter[4] = String.valueOf(FragmentCupGroupThreeRanking.listRankingGroupThree.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[5] = String.valueOf(FragmentCupGroupFourRanking.listRankingGroupFour.get(1).get(0));
                // 1er G4 vs 2eme G3
                ActivityTournament.playerNameQualifiedQuarter[6] = String.valueOf(FragmentCupGroupFourRanking.listRankingGroupFour.get(0).get(0));
                ActivityTournament.playerNameQualifiedQuarter[7] = String.valueOf(FragmentCupGroupThreeRanking.listRankingGroupThree.get(1).get(0));

                // Changing the number of players for adaptation with ActivityTournament
                FragmentSelectTournament.convertInputUserNumberPlayer = 8;
            }

            // Change the key type of tournament
            DetailPopupTemplate.keyTypeTournament = FragmentSelectTournament.typeTournament[3];
        }
    };
}