package com.projetat.gamegenius.builder.components.activity;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import androidx.navigation.NavController;

import com.projetat.gamegenius.R;
import com.projetat.gamegenius.builder.components.popup.DetailPopupTemplate;

import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ActivityTournamentTest {

    @Mock
    NavController mockNavController;

    ActivityTournament activity;
    @Before
    public void setUp() {
        activity = new ActivityTournament();
        activity.navController = mockNavController;
    }
    @Test
    public void testDisplayForTournament() {

        // Vérifie que les destionations sont bien atteinte
        activity.displayForTournament(16);
        verify(mockNavController).navigate(R.id.fragmentTournamentEighth);

        activity.displayForTournament(8);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        activity.displayForTournament(4);
        verify(mockNavController).navigate(R.id.fragmentTournamentSemi);

        activity.displayForTournament(2);
        verify(mockNavController).navigate(R.id.fragmentTournamentFinal);

        // Vérifie que certaines destionations ne sont pas atteinte
        activity.displayForTournament(4);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentFinal);

        activity.displayForTournament(2);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentSemi);
    }

    @Test
    public void testDisplayForCup() {
        // Vérifie que les destionations sont bien atteinte
        activity.displayForCup(4);
        verify(mockNavController).navigate(R.id.fragmentTournamentSemi);

        activity.displayForCup(8);
        verify(mockNavController).navigate(R.id.fragmentTournamentSemi);

        activity.displayForCup(9);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        activity.displayForCup(12);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        activity.displayForCup(13);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        activity.displayForCup(20);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        // Vérifie que certaines destionations ne sont pas atteinte
        activity.displayForCup(8);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentQuarter);

        activity.displayForCup(9);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentSemi);

    }

    @Test
    public void testDisplayFragmentForTournamentType() {
        // Vérifie que les destionations sont bien atteinte
        DetailPopupTemplate.keyTypeTournament = "tournament";
        activity.displayFragmentTournament(16);
        verify(mockNavController).navigate(R.id.fragmentTournamentEighth);

        DetailPopupTemplate.keyTypeTournament = "cup";
        activity.displayFragmentTournament(16);
        verify(mockNavController).navigate(R.id.fragmentTournamentQuarter);

        // Vérifie que certaines destionations ne sont pas atteinte
        DetailPopupTemplate.keyTypeTournament = "tournament";
        activity.displayFragmentTournament(16);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentQuarter);

        DetailPopupTemplate.keyTypeTournament = "cup";
        activity.displayFragmentTournament(16);
        verify(mockNavController, times(0)).navigate(R.id.fragmentTournamentEighth);

    }
}