package com.projetat.gamegenius;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.test.core.app.ApplicationProvider;

import com.projetat.gamegenius.builder.components.activity.ActivityWinner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = 28,
        manifest = "src/main/AndroidManifest.xml",
        resourceDir = "src/main/res")
public class ActivityWinnerTest {

    private ActivityWinner activityWinner;

    @Before
    public void setUp() {
        activityWinner = Robolectric.buildActivity(ActivityWinner.class).create().get();
    }

    // Esssayer de mettre le before a jour pour faire fonctionner le test
    /*@Test
    public void testAllReset() {
        activityWinner.allReset();

        assertNull(FragmentHomePage.inputNameTournament);
        assertNull(FragmentSelectTournament.inputUserNumberPlayer);
        assertEquals(0, FragmentSelectTournament.convertInputUserNumberPlayer);
        assertNull(FragmentInputNamePlayer.name);
        assertTrue(FragmentInputNamePlayer.inputUserList.isEmpty());
        assertTrue(FragmentInputNamePlayer.playerNameList.isEmpty());
        assertArrayEquals(new String[4], ActivityTournament.playerNameQualifiedEighth);
        assertArrayEquals(new String[4], ActivityTournament.playerNameQualifiedQuarter);
        assertArrayEquals(new String[4], ActivityTournament.playerNameQualifiedSemi);
        assertArrayEquals(new String[4], ActivityTournament.playerNameQualifiedFinal);
        assertArrayEquals(new String[4], ActivityTournament.playerNameWinner);
        assertEquals("", ActivityCup.keyGroup);
        assertTrue(FragmentCupGroupOneRanking.listRankingGroupOne.isEmpty());
        assertTrue(FragmentCupGroupTwoRanking.listRankingGroupTwo.isEmpty());
        assertTrue(FragmentCupGroupThreeRanking.listRankingGroupThree.isEmpty());
        assertTrue(FragmentCupGroupFourRanking.listRankingGroupFour.isEmpty());
        assertNull(DetailPopupTemplate.keyTypeTournament);
        assertEquals(0, MatchPopupTemplate.token);
    }*/

    @Test
    public void testClearCache() throws IOException {
        File cacheDir = activityWinner.getCacheDir();
        File tempFile = new File(cacheDir, "tempFile.txt");
        assertTrue(tempFile.createNewFile());

        assertTrue(tempFile.exists());
        activityWinner.clearCache();
        assertFalse(tempFile.exists());
    }

    @Test
    public void testDeleteDir() throws IOException {
        File tempDir = new File(activityWinner.getCacheDir(), "tempDir");
        assertTrue(tempDir.mkdir());
        File tempFile = new File(tempDir, "tempFile.txt");
        assertTrue(tempFile.createNewFile());

        assertTrue(tempDir.exists());
        assertTrue(tempFile.exists());

        assertTrue(activityWinner.deleteDir(tempDir));
        assertFalse(tempDir.exists());
        assertFalse(tempFile.exists());
    }

    @Test
    public void testClearSharedPreferences() {
        Context context = ApplicationProvider.getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("your_preferences_name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key", "value");
        editor.apply();

        assertEquals("value", sharedPreferences.getString("key", null));

        activityWinner.clearSharedPreferences();
        assertNull(sharedPreferences.getString("key", null));
    }
}

