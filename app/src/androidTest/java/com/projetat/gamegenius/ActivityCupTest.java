package com.projetat.gamegenius;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.activity.ActivityCup;
import com.projetat.gamegenius.builder.components.fragment.FragmentSelectTournament;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ActivityCupTest {

    @Rule
    public ActivityScenarioRule<ActivityCup> activityRule =
            new ActivityScenarioRule<>(ActivityCup.class);

    @Test
    public void testTitleCupDisplayed() {
        // Check if the title is correctly displayed
        Espresso.onView(withId(R.id.titleCup))
                .check(matches(withText("")));
    }

    @Test
    public void testTabLayoutDisplayed() {
        // Check if the TabLayout is displayed with the correct number of tabs
        Espresso.onView(withId(R.id.tabLayoutCup))
                .check(matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withText("Poule 1"))
                .check(matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withText("Poule 2"))
                .check(matches(ViewMatchers.isDisplayed()));

        // For number of players > 12
        if (FragmentSelectTournament.convertInputUserNumberPlayer > 12) {
            Espresso.onView(ViewMatchers.withText("Poule 3"))
                    .check(matches(ViewMatchers.isDisplayed()));
            Espresso.onView(ViewMatchers.withText("Poule 4"))
                    .check(matches(ViewMatchers.isDisplayed()));
        }
    }

    @Test
    public void testTabSelectionUpdatesKeyGroup() {
        // Select different tabs and check if keyGroup is updated correctly
        Espresso.onView(ViewMatchers.withText("Poule 2")).perform(ViewActions.click());
        Espresso.onView(withId(R.id.viewFragmentCup)).check((view, noViewFoundException) -> {
            assert ActivityCup.keyGroup.equals("Group_two");
        });

        // For number of players > 12
        if (FragmentSelectTournament.convertInputUserNumberPlayer > 12) {
            Espresso.onView(ViewMatchers.withText("Poule 3")).perform(ViewActions.click());
            Espresso.onView(withId(R.id.viewFragmentCup)).check((view, noViewFoundException) -> {
                assert ActivityCup.keyGroup.equals("Group_three");
            });

            Espresso.onView(ViewMatchers.withText("Poule 4")).perform(ViewActions.click());
            Espresso.onView(withId(R.id.viewFragmentCup)).check((view, noViewFoundException) -> {
                assert ActivityCup.keyGroup.equals("Group_four");
            });
        }

        Espresso.onView(ViewMatchers.withText("Poule 1")).perform(ViewActions.click());
        Espresso.onView(withId(R.id.viewFragmentCup)).check((view, noViewFoundException) -> {
            assert ActivityCup.keyGroup.equals("Group_one");
        });
    }

    @Test
    public void testBackButtonDisabled() {
        // Press the back button and verify that the activity does not close
        Espresso.pressBack();
        // The activity should still be in the foreground
        ActivityScenario<ActivityCup> scenario = ActivityScenario.launch(ActivityCup.class);
        scenario.onActivity(activity -> {
            assert activity != null;
        });
    }
}

