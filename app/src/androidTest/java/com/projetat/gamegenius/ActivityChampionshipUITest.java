package com.projetat.gamegenius;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.junit.Assert.assertNotNull;

import androidx.test.core.app.ActivityScenario;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.activity.ActivityChampionship;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ActivityChampionshipUITest {

    // Create Senario Rule
    @Rule
    public ActivityScenarioRule<ActivityChampionship> activityRule =
            new ActivityScenarioRule<>(ActivityChampionship.class);

    // Check if Tab display
    @Test
    public void testTabLayoutIsDisplayed() {
        onView(withId(R.id.tabLayout))
                .check(matches(isDisplayed()));
    }

    // Check if Tab display with text "Matchs" and check if tab display
    @Test
    public void testTabsSwitching() {
        onView(withText("Matchs")).perform(click());
        onView(withId(R.id.viewFragmentChampionship))
                .check(matches(isDisplayed()));
    }

    // Check if Tab display with text "Classement" and check if not null
    @Test
    public void testFragmentTransaction() {
        onView(withText("Classement")).perform(click());
        ActivityScenario<ActivityChampionship> fragment = activityRule.getScenario();
        assertNotNull(fragment);
    }
}

