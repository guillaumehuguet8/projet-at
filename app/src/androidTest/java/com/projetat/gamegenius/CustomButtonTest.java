package com.projetat.gamegenius;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.custom.CustomButton;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class CustomButtonTest {

    private Context context;
    private LinearLayout layout;
    private CustomButton customButton;
    private View.OnClickListener listener;

    // Context, Layout, Listener object initialization
    @Before
    public void setUp() {
        context = ApplicationProvider.getApplicationContext();
        layout = new LinearLayout(context);
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Click action
            }
        };
        customButton = new CustomButton(context, layout, listener);
    }

    // Check that the button is correctly initialized and added to the layout
    @Test
    public void testConstructorButtonStandard() {
        customButton.constructorButtonStandard("Test Button");
        Button button = (Button) layout.getChildAt(0);

        assertNotNull(button);
        assertEquals("Test Button", button.getText().toString());
        assertEquals(Typeface.createFromAsset(context.getResources().getAssets(), "fonts/contrailone.ttf"), button.getTypeface());
        assertEquals(View.VISIBLE, button.getVisibility());
    }

    // Check that the button background is correctly updated
    @Test
    public void testSetColorBackgroundButtonStandard() {
        customButton.constructorButtonStandard("Test Button");
        customButton.setColorBackgroundButtonStandard(R.drawable.corner_round);

        Button button = (Button) layout.getChildAt(0);
        Drawable expectedDrawable = AppCompatResources.getDrawable(context, R.drawable.corner_round);
        Drawable actualDrawable = button.getBackground();

        // Ensure that both drawables are non-null
        assertNotNull(expectedDrawable);
        assertNotNull(actualDrawable);

        // Compare the two drawables
        assertTrue(expectedDrawable.getConstantState().equals(actualDrawable.getConstantState()));
    }

    // Check that button visibility is correctly updated
    @Test
    public void testSetVisibilityButtonNext() {
        customButton.constructorButtonNext();
        customButton.setVisibilityButtonNext();

        Button button = (Button) layout.getChildAt(0);
        assertEquals(View.VISIBLE, button.getVisibility());
    }

    // Refaire une fois que la désactivation marchera
    /*@Test
    public void testSetDeactivateValidationScore() {
        customButton.constructorButtonStandard("Test Button");
        customButton.setDeactivateValidationScore();

        Button button = (Button) layout.getChildAt(0);
        assertFalse(button.isClickable());
        assertFalse(button.isEnabled());
    }*/

    // Check that the match button is correctly initialized and added to the layout
    @Test
    public void testConstructorButtonMatch() {
        customButton.constructorButtonMatch("Player 1", "Player 2", 123);
        Button button = (Button) layout.getChildAt(0);

        assertNotNull(button);
        assertEquals("Player 1     VS     Player 2", button.getText().toString());
        assertEquals(123, button.getId());
    }
}