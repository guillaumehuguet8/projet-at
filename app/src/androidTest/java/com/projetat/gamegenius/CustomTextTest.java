package com.projetat.gamegenius;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.projetat.gamegenius.builder.components.custom.CustomText;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class CustomTextTest {

    private Context context;
    private LinearLayout layout;
    private CustomText customText;
    private static final String TEST_TEXT = "Test Text";

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        layout = new LinearLayout(context);
        customText = new CustomText(context, TEST_TEXT, layout);
    }

    @Test
    public void testConstructorTitle() {
        customText.constructorTitle();
        TextView title = (TextView) layout.getChildAt(0);

        assertNotNull(title);
        // Ensure the view is fully initialized and updated
        title.post(() -> {
            assertEquals(TEST_TEXT, title.getText().toString());
            assertTrue(title.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, title.getTextAlignment());
        });

        // Check Typeface
        assertEquals(Typeface.createFromAsset(context.getAssets(), "fonts/anton.ttf"), title.getTypeface());
    }

    @Test
    public void testConstructorSubtitle() {
        customText.constructorSubtitle();
        TextView subtitle = (TextView) layout.getChildAt(0);

        assertNotNull(subtitle);
        // Ensure the view is fully initialized and updated
        subtitle.post(() -> {
            assertEquals(TEST_TEXT, subtitle.getText().toString());
            assertTrue(subtitle.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, subtitle.getTextAlignment());
        });

        // Check Typeface
        assertEquals(Typeface.createFromAsset(context.getAssets(), "fonts/contrailone.ttf"), subtitle.getTypeface());
    }

    @Test
    public void testConstructorText() {
        customText.constructorText();
        TextView allText = (TextView) layout.getChildAt(0);

        assertNotNull(allText);
        // Ensure the view is fully initialized and updated
        allText.post(() -> {
            assertEquals(TEST_TEXT, allText.getText().toString());
            assertTrue(allText.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, allText.getTextAlignment());
        });

        // Check Typeface
        assertEquals(Typeface.createFromAsset(context.getAssets(), "fonts/roboto.ttf"), allText.getTypeface());
    }

    @Test
    public void testConstructorHeaderTextRank() {
        customText.constructorHeaderTextRank();
        TextView headerTextRank = (TextView) layout.getChildAt(0);

        assertNotNull(headerTextRank);
        // Ensure the view is fully initialized and updated
        headerTextRank.post(() -> {
            assertEquals(TEST_TEXT, headerTextRank.getText().toString());
            assertTrue(headerTextRank.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, headerTextRank.getTextAlignment());
        });

        // Check Typeface
        assertEquals(Typeface.createFromAsset(context.getAssets(), "fonts/roboto.ttf"), headerTextRank.getTypeface());

        // Check Background Resource (Mocking)
        assertEquals(R.drawable.rectangle_header_ranking, headerTextRank.getBackground().getConstantState());
        assertEquals(context.getResources().getColor(R.color.white), headerTextRank.getCurrentTextColor());
    }

    @Test
    public void testConstructorTextRank() {
        customText.constructorTextRank();
        TextView textRank = (TextView) layout.getChildAt(0);

        assertNotNull(textRank);
        // Ensure the view is fully initialized and updated
        textRank.post(() -> {
            assertEquals(TEST_TEXT, textRank.getText().toString());
            assertTrue(textRank.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, textRank.getTextAlignment());
        });

        // Check Typeface
        assertEquals(Typeface.createFromAsset(context.getAssets(), "fonts/roboto.ttf"), textRank.getTypeface());

        // Check Background Resource (Mocking)
        assertEquals(R.drawable.rectangle_ranking, textRank.getBackground().getConstantState());
        assertEquals(context.getResources().getColor(R.color.black), textRank.getCurrentTextColor());
    }
}