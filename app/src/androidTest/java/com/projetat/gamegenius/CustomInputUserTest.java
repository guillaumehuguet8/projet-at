package com.projetat.gamegenius;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.custom.CustomInputUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class CustomInputUserTest {

    private Context context;
    private LinearLayout layout;
    private CustomInputUser customInputUser;

    @Before
    public void setUp() {
        context = ApplicationProvider.getApplicationContext();
        layout = new LinearLayout(context);
        customInputUser = new CustomInputUser(context, layout);
    }

    @Test
    public void testConstructorEditText() {
        String hint = "Enter text";
        customInputUser.constructorEditText(hint);
        EditText editText = (EditText) layout.getChildAt(0);

        assertNotNull(editText);
        assertEquals(hint, editText.getHint().toString());
        // Check the length filter
        InputFilter[] filters = editText.getFilters();
        boolean lengthFilterFound = false;
        for (InputFilter filter : filters) {
            if (filter instanceof InputFilter.LengthFilter) {
                lengthFilterFound = true;
                break;
            }
        }
        assertTrue(lengthFilterFound);
        // Verify the text size is set (not the exact value due to screen density variations)
        assertTrue(editText.getTextSize() > 0);
    }

    @Test
    public void testConstructorEditTextNamePlayer() {
        int playerNumber = 1;
        customInputUser.constructorEditTextNamePlayer(playerNumber);
        EditText editText = (EditText) layout.getChildAt(0);

        assertNotNull(editText);
        assertEquals("Joueur " + playerNumber, editText.getHint().toString());
        // Check the length filter
        InputFilter[] filters = editText.getFilters();
        boolean lengthFilterFound = false;
        for (InputFilter filter : filters) {
            if (filter instanceof InputFilter.LengthFilter) {
                lengthFilterFound = true;
                break;
            }
        }
        assertTrue(lengthFilterFound);
        // Verify the text size is set
        assertTrue(editText.getTextSize() > 0);
        assertEquals(playerNumber, editText.getId());
    }

    @Test
    public void testConstructorEditNumber() {
        String hint = "Enter number";
        customInputUser.constructorEditNumber(hint);
        EditText editText = (EditText) layout.getChildAt(0);

        assertNotNull(editText);
        assertEquals(hint, editText.getHint().toString());
        // Check the length filter
        InputFilter[] filters = editText.getFilters();
        boolean lengthFilterFound = false;
        for (InputFilter filter : filters) {
            if (filter instanceof InputFilter.LengthFilter) {
                lengthFilterFound = true;
                break;
            }
        }
        assertTrue(lengthFilterFound);
        assertEquals(Color.LTGRAY, ((ColorDrawable) editText.getBackground()).getColor());

        // Ensure the view is fully initialized and updated
        editText.post(() -> {
            assertEquals(InputType.TYPE_CLASS_NUMBER, editText.getInputType());
            assertTrue(editText.getTextSize() > 0);
            assertEquals(View.TEXT_ALIGNMENT_CENTER, editText.getTextAlignment());
        });
    }

    @Test
    public void testGetTextEditText() {
        String expectedText = "Sample Text";
        customInputUser.constructorEditText("Hint");
        EditText editText = (EditText) layout.getChildAt(0);
        editText.setText(expectedText);

        assertEquals(expectedText, customInputUser.getTextEditText());
    }

    @Test
    public void testGetEditNumber() {
        String expectedNumber = "123";
        customInputUser.constructorEditNumber("Hint");
        EditText editText = (EditText) layout.getChildAt(0);
        editText.setText(expectedNumber);

        assertEquals(expectedNumber, customInputUser.getEditNumber());
    }

    @Test
    public void testGetEditNamePlayer() {
        String expectedName = "Player1";
        customInputUser.constructorEditTextNamePlayer(1);
        EditText editText = (EditText) layout.getChildAt(0);
        editText.setText(expectedName);

        assertEquals(expectedName, customInputUser.getEditNamePlayer());
    }

    @Test
    public void testSetTextEditNumber() {
        String newText = "456";
        customInputUser.constructorEditNumber("Hint");
        customInputUser.setTextEditNumber(newText);

        EditText editText = (EditText) layout.getChildAt(0);
        assertEquals(newText, editText.getText().toString());
    }
}
