package com.projetat.gamegenius;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import static org.junit.Assert.assertNotNull;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.activity.ActivityTournament;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ActivityTournamentTest {
    @Rule
    public ActivityScenarioRule<ActivityTournament> activityScenarioRule =
            new ActivityScenarioRule<>(ActivityTournament.class);

    @Test
    public void toolbarIsDisplayed() {
        onView(withId(R.id.my_toolbar_tournament))
                .check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void navControllerIsSet() {
        // Lancer le scénario de l'activité et obtenir l'instance d'ActivityScenario
        activityScenarioRule.getScenario().onActivity(activity -> {
            // Obtenir le NavController
            NavController navController = Navigation.findNavController(activity, R.id.fragment_container_view_tournament);

            // Vérifier que le NavController n'est pas nul
            assertNotNull(navController);
        });
    }

}
