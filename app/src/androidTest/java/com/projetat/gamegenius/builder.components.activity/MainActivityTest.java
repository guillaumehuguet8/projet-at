package com.projetat.gamegenius.builder.components.activity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule =
            new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void toolbarIsDisplayed() {
        onView(withId(R.id.my_toolbar_main))
                .check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void navControllerIsSet() {
        // Lancer le scénario de l'activité et obtenir l'instance d'ActivityScenario
        activityScenarioRule.getScenario().onActivity(activity -> {
            // Obtenir le NavController
            NavController navController = Navigation.findNavController(activity, R.id.fragment_container_view_main);

            // Vérifier que le NavController n'est pas nul
            assertNotNull(navController);
        });
    }

    //TODO: androidx.test.espresso.AmbiguousViewMatcherException
    @Test
    public void testNavigationOnToolbarClick() {
        // Suppose there is a navigation item with an id `nav_item` in the toolbar
        onView(withId(R.id.my_toolbar_main)).perform(click());

        // Check that the navigation has occurred, perhaps by checking a fragment's view
        onView(withId(R.id.fragment_container_view_main)).check(ViewAssertions.matches(isDisplayed()));
    }
}
