package com.projetat.gamegenius;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.projetat.gamegenius.builder.components.activity.ActivityWinner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class ActivityWinnerTest {

    @Rule
    public ActivityScenarioRule<ActivityWinner> activityRule =
            new ActivityScenarioRule<>(ActivityWinner.class);

    @Test
    public void testActivityLaunches() {
        // Test if the activity launches and the title is displayed correctly
        Espresso.onView(withId(R.id.titleWinner))
                .check(matches(withText("")));
    }

    /*@Test
    public void testWinnerNameDisplayed() {
        // Test if the winner's name is displayed correctly
        Espresso.onView(withId(R.id.winner))
                .check(matches(withText("")));
    }*/

    @Test
    public void testResetButton() {
        // Click on the reset button and check if MainActivity is launched
        Espresso.onView(withText("Reset")).perform(click());

        // You can use Intents to verify that MainActivity is started
        // intended(hasComponent(MainActivity.class.getName()));
    }

    @Test
    public void testBackButtonDisabled() {
        // Press back button and verify the activity is not finished
        Espresso.pressBack();

        // The activity should still be in the foreground
        ActivityScenario<ActivityWinner> scenario = ActivityScenario.launch(ActivityWinner.class);
        scenario.onActivity(activity -> {
            assert activity != null;
        });
    }
}

